"""Tests that all the modules in the package can be imported without error.
"""
import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'bnf_download.bnf',
        'bnf_download.chembl_orm',
        'bnf_download.download',
        'bnf_download.orm',
        'bnf_download.queries'
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
