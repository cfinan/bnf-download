.. file-indexer documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bnf-download
=======================

The `bnf-download <https://gitlab.com/cfinan/bnf-download>`_ package contains classes and scripts for downloading (scraping) the drug indication and side effect information from the BNF database on the `BNF website <https://bnf.nice.org.uk/>`_. This is necessary as no downloadable flat file exists. Note that this is the free version and not the paid one at `medicines complete <https://about.medicinescomplete.com/>`_. This free version does not have any BNF chapters for example.

This package uses SQLAlchemy to manage the connections to all of the databases, as such, it is not restricted to any single database management system (DBMS). Having said that, currently it has only been tested against SQLite and MariaDB.

Not only does it download the content from the BNF website. It also attempts to link the drug names back to ChEMBL identifiers. This then opens up the possibility for look ups on drug data. From the ChEMBL ID, the efficacy target for the drug is then extracted (as uniprot IDs). The uniprot IDs are mapped against Ensembl (using the REST API). to provide full drug->protein target->gene linkage.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
