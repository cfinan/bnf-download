==============
Python Scripts
==============

``bnf-download``
----------------

.. argparse::
   :module: bnf_download.download
   :func: _init_cmd_args
   :prog: bnf-download

Example usage
~~~~~~~~~~~~~

Here are some examples usage.

No ChEMBL mapping and default SQLite output
...........................................

Here a ChEMBL connection has not been specified and neither has any db argument. Therefore, only the contents of the `BNF website` <https://bnf.nice.org.uk/>`_ will be downloaded and the database will be an SQLite database situated ``~/``. We also have verbosity (``-v``) switched on.

.. code-block:: console

   $ bnf-download -v


No ChEMBL mapping but specify an output SQLite database
.......................................................

Here an alternate filename and location for the SQLite database is given.

.. code-block:: console

   $ bnf-download -v /path/to/my_bnf_sqlite.db

Specifying ChEMBL as an SQLite database
.......................................

Here the location of a SQLite version of ChEMBL is given. So both ChEMBL mapping and Ensembl mapping (using GRCh37) will take place. See below for more information about ChEMBL and changing genome build.

.. code-block:: console

   $ bnf-download -v -m /path/to/chembl_27.db /path/to/my_bnf_sqlite.db


Using GRCh38 instead of GRCh37
..............................

Here we have altered the REST server to use the version for genome build GRCh38

.. code-block:: console

   $ bnf-download -v -e "https://rest.ensembl.org" -m /path/to/chembl_27.db /path/to/my_bnf_sqlite.db

In a rush and do not want to wait
.................................

Here we take the maximum random wait time down to 0 so the web queries happen directly after each other. This will take the download time from ~30 mins to about ~5 mins

.. code-block:: console

   $ bnf-download -w0 -v

Using non SQLite data sources
.............................

Through `SQLAlchemy <https://www.sqlalchemy.org/>`_ it is possible to use other DBMS. As most DBMS will require a password it is not a great idea to pass a raw connection sting on the command line. There for, the `--chembl` and the `db` parameters can accept a config file section header as arguments. The config file is in a standard `ini` format.

.. code-block:: console

   $ bnf-download -v -m chembl_27 bnf_latest

The location of the config file can be specified by the `--config` argument. If this is not specified by the default the config file is assumed to be `~/.bnf.cnf`. An example of the format of the config file is shown below:

The config file
~~~~~~~~~~~~~~~

An example config file is shown below:

.. code-block:: ini

   # Make sure no one else can see this file but you
   # i.e.chmod o-rwx ~/.bnf.cnf

   # Make sure passwords are URL escaped:
   # import urllib.parse
   # PW = urllib.parse.quote_plus(PW)
   # See below for info in connection URLS:
   # https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
   # Also, don't forget to escape any % that are in the URL (with a second %)

   # The section headers can be called anything
   [bnf_latest]
   # All connection options here should be prefixed with bnf.
   bnf.url=mysql+pymysql://user:password@hostname:3306/bnf

   # You can still use SQLite databases from the config file
   [chembl_27]
   # All connection options here should be prefixed with chembl.
   chembl.url=sqlite:////path/to/chembl_27.db

   [chembl_25]
   # All connection options here should be prefixed with chembl.
   chembl.url=mysql+pymysql://user:password@hostname:3306/chembl_25

Known issues
~~~~~~~~~~~~

When running you will get warning messages about 0 side effects for specific drugs. As far as I am aware these will be genuine. I have noticed that some combination drugs have 0 side effects where as there individual components do have side effects, so where possible use the side effect data from individual components and not combinations only.

There are also one or two 'drugs' with no indications, on closer inspection, these are also valid and are things such as `grapefruit <https://bnf.nice.org.uk/drugs/grapefruit/>`_.

So far this script has only been tested against SQLite and MariaDB/MySQL. It is possibly that some of the ORM classes will need to be updated to get it to work with other databases.
