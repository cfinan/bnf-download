==========
BNF schema
==========

The schema used by bnf-download is shown below.

.. image:: ./_static/bnf_schema.png
   :width: 500
   :alt: BNF schema
   :align: center

.. include:: ./data_dict/bnf-tables.rst
