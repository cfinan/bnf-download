``bnf_download`` package
========================

``bnf_download.download`` module
--------------------------------

.. autofunction:: bnf_download.download.download_bnf


``bnf_download.queries`` module
-------------------------------

.. automodule:: bnf_download.queries
   :members:
   :undoc-members:
   :show-inheritance:


``bnf_download.bnf`` module
----------------------------

.. automodule:: bnf_download.bnf
   :members:
   :undoc-members:
   :show-inheritance:

.. include:: ../data_dict/bnf_orm_api.rst
