"""Classes for querying the BNF database.
"""
from bnf_download import orm as o
from sqlalchemy import text
from umls_tools.admin import index
import warnings


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BnfQuery(object):
    """Query the BNF database,

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQLAlchemy session object to issue queries.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, session):
        self.session = session
        nterms = self.session.query(o.DrugEffect.drug_effect_id).count()
        self.index_search = index.BaseSearchIndex(session, o, nterms)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_bnf_date(self):
        """Get the BNF update version date from the database.

        Returns
        -------
        version : `datetime.datetime`
            The BNF version date.
        """
        return self.session.query(o.Version.version_date).one()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_all_drugs(self):
        """Return all the drugs in the database.

        Returns
        -------
        drugs : `list` of `bnf_download.orm.Drug`
            The drug records in the database.
        """
        return self.session.query(o.Drug).all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_all_drug_components(self):
        """Return all the drugs in the database.

        Returns
        -------
        drugs : `list` of `bnf_download.orm.Drug`
            The drug records in the database.
        """
        return self.session.query(o.DrugComponent).all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_unique_target_accessions(self):
        """get a list of unique Uniprot IDs for efficacy targets of drugs in
        ChEMBL that have been mapped to BNF drugs.

        Returns
        -------
        uniprot_ids : `list` of `str`
            Unique uniprot IDs.
        """
        return [
            i.target_accession for i in self.session.query(
                o.ChemblTargetComponents.target_accession
            ).group_by(
                o.ChemblTargetComponents.target_accession
            ).all()
        ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_zero_indications(self):
        """Return a list of the drugs with no indication data. Some of these
        could be legit cases others maybe link out errors.

        Returns
        -------
        drugs : `list` of `bnf_download.orm.Drug`
            The drug records in the database.
        """
        # Left
        # https://stackoverflow.com/questions/11144536
        # /sqlalchemy-join-child-table-with-2-conditions
        # q = self.session.query(o.Drug).\
        #     outerjoin(o.DrugEffect,
        #               and_(o.DrugEffect.drug_id == o.Drug.drug_id,
        #                    o.DrugEffect == 'indication')).\
        #     filter(o.DrugEffect.drug_effect_id == None)
        q = self.session.query(
            o.Drug
        ).outerjoin(
            o.DrugEffect,
            (o.DrugEffect.drug_id == o.Drug.drug_id)
            &
            (o.DrugEffect.drug_effect_type == 'indication')
        ).filter(
            o.DrugEffect.drug_effect_id == None
        )

        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_zero_side_effects(self):
        """Return a list of the drugs with no side effect data. Some of these
        could be legit cases others maybe link out errors.

        Returns
        -------
        drugs : `list` of `bnf_download.orm.Drug`
            The drug records in the database.
        """
        q = self.session.query(
            o.Drug
        ).outerjoin(
            o.DrugEffect,
            (o.DrugEffect.drug_id == o.Drug.drug_id)
            &
            (o.DrugEffect.drug_effect_type == 'side_effect')
        ).filter(o.DrugEffect.drug_effect_id == None)

        return q.all()

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def get_unmapped_effects(self):
    #     """Return drug effects that have not been mapped to the UMLS.

    #     Returns
    #     -------
    #     drugs : `list` of (`bnf_download.orm.Drug`, \
    #     bnf_download.orm.DrugEffect)
    #         The drug records in the database.
    #     """
    #     q = self.session.query(
    #         o.Drug, o.DrugEffect
    #     ).outerjoin(
    #         o.DrugEffectMapping
    #     ).filter(
    #         o.DrugEffectMapping.drug_effect_mapping_id == None
    #     )
    #     return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_drugs_with_effect(self, drug_effect):
        """Serach the drug effect index table for drugs that match the given
        drug effect string.

        Parameters
        ----------
        drug_effect : `str`
            The drug effect to search for.

        Returns
        -------
        matching_drugs : `list` or `list`
            The elements are ordered from highest score to lowest and each
            sub-list  (matching term) has the following elements:

            0. The match score source_coverage * target_coverage *
               sum(log10(total index terms/token freq)).
            1. The length of the match.
            2. The length of the whole matching term
            3. A set of tuples of the containing the matching positions of
               individual tokens.
            4. The token mapping rows in the database (SQLAlchemy objects)

        Notes
        -----
        The drug effect argument is processed in the same way the drug effects
        that built the index table are processed. The best matches are
        returned.
        """
        return self.index_search.search_term(drug_effect)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_target_effects(self, chembl_target_id):
        """Get the target effects, this needs re-writing in ORM code.

        Parameters
        ----------
        chembl_target_id : `str`
            A ChEMBL target identifier to search for.

        Returns
        -------
        target_effects : `list`
            All the target effects for the ChEMBL target.
        """
        sql = text(
            """
            select  cm.compound_chembl_id
            , cm.mapping_synonym
            , cm.compound_pref_name
            , cm.molecule_type
            , cm.full_mapping
            , d.drug_name as bnf_drug_name
            , ctc.target_mapping_relationship
            , ctc.mechanism_of_action
            , ctc.target_chembl_id
            , ctc.target_type
            , ctc.target_pref_name
            , ctc.action_type
            , ctc.uniprot_accession
            , ctc.uniprot_description
            , de.drug_effect_type
            , de.drug_effect_freq
            , de.drug_effect_name
            , de.drug_class_effect
            from chembl_target_components ctc
            join chembl_mapping cm
            on ctc.chembl_mapping_id = cm.chembl_mapping_id
            join drug_component dc
            on cm.drug_component_id = dc.drug_component_id
            join drug d on dc.drug_id = d.drug_id
            join drug_effect de on d.drug_id = de.drug_id
            where target_chembl_id = :CHEMBL_ID
            """
        )
        query = self.session.execute(sql, {'CHEMBL_ID': chembl_target_id})
        return query.fetchall()


# #############################################################################
#                    SELECTED QUERIES AGAINST THE BNF                         #
# #############################################################################


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def select_all_drug_componenets(session):
    """Select all the drug components.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        SQL alchemy session to the BNF database.

    Returns
    -------
    """
    warnings.warn(
        "Deprecated use BnfQuery.get_all_drug_components instead",
        DeprecationWarning
    )
    return session.query(o.DrugComponent).all()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def select_unique_uniprot_targets(session):
    """get a list of unique Uniprot IDs for efficacy targets of drugs in ChEMBL
    that have been mapped to BNF drugs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        SQL alchemy session to the BNF database.

    Returns
    -------
    uniprot_ids : `list` of `str`
        Unique uniprot IDs.
    """
    warnings.warn(
        "Deprecated use BnfQuery.get_unique_target_accessions instead",
        DeprecationWarning
    )
    return [i.uniprot_accession for i in session.query(
        o.ChemblTargetComponents.uniprot_accession).\
        group_by(o.ChemblTargetComponents.uniprot_accession).all()]
