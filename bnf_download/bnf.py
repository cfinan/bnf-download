"""Classes to handle the scraped BNF data.
"""
# from umls_tools import metamap
from lxml import html, etree
from pyaddons import utils
from datetime import datetime
import requests
import webbrowser
import re
# import unicodedata
# import pprint as pp


# Note to see the HTML from a request use:
# tree = html.fromstring(r.content)
# print(etree.tostring(tree, encoding='unicode', pretty_print=True))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BnfScrape(object):
    """Main controller class for scraping.
    """
    BNF_URL = "https://bnf.nice.org.uk"
    """The URL entry point for BNF (`str`)
    """
    DRUGS_LIST_URL = f"{BNF_URL}/drug/"
    """The URL entry point for BNF drug links (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_update_date(self):
        """Get the date and time when the website was last updated.

        Returns
        -------
        update_date : `datetime.datetime`
            The update date from the BNF website.
        """
        r = requests.get(self.BNF_URL)
        r.raise_for_status()
        tree = html.fromstring(r.content)
        tree.make_links_absolute(r.url)
        update_field = tree.xpath(
            '//div[starts-with(@class, "Hero-module--lastUpdated-")]/time'
        )
        if len(update_field) != 1:
            raise KeyError("can't find update field")

        # i.e. "2023-07-26T22:39:15.139"
        return datetime.fromisoformat(update_field[0].get("datetime"))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_drug_links(self):
        """Get all the drug links from the main drugs page.

        Returns
        -------
        drug_links : `list` of `bnf_download.bnf.DrugLink`
            The names and links to the drug pages.

        Rasies
        ------
        RuntimeError
            If the no drug links can be found or if no drug names can be
            detected.
        """
        drug_links = []

        # Go to the drug list page
        r = requests.get(self.__class__.DRUGS_LIST_URL)
        r.raise_for_status()

        # Now parse the drugs out of the page
        tree = html.fromstring(r.content)
        tree.make_links_absolute(r.url)
        for i in tree.xpath('//li[@class="a-z-list__item "]//li/a'):
            url = i.attrib['href']
            drug = i.text.strip()
            if drug == '':
                raise RuntimeError(
                    "no drug name found, has the website changed?"
                )
            drug_links.append(DrugLink(drug, url))

        if len(drug_links) == 0:
            raise RuntimeError(
                "no drug links found, has the website changed?"
            )
        return drug_links

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_drug(self, drug_link):
        """Process a formulation. A formulation could be a mixture of two or
        more drug components.

        Parameters
        ----------
        treat_link : `bnf_download.bnf.DrugLink`
            The location of the treatment webpage.

        Returns
        -------
        drug : `bnf_download.bnf.Drug`
            The parsed drug. This is indications, side effects, cautions,
            contra-indications and ROA/dosage info.

        Raises
        ------
        RequestsError
            If the web request gives a bad response code.
        """
        drug_page = drug_link.get_drug_page()

        try:
            # First we xpath out the treatment title and them process it into
            # components
            drug_data = drug_page.xpath(
                '//body//main//div[@id="content-start"]'
                '/h1[@class="page-header__heading "]/span/text()'
            )[0]
        except IndexError as e:
            # This will mean that the xpath above has not come back with
            # anything and the structure might have changed
            drug_link.print_drug_page()
            raise e.__class__("error accessing: {0}".format(drug_link.url))

        # Remove unwanted strings from the drug name
        drug_data = self.clean_drug_name(drug_data)

        # Now get the treatment components
        components = self.parse_drug_components(drug_data)

        # Parse out indications and roa and dosages
        indications, synonyms = self.parse_indication_roa(
            drug_page, drug_link.drug_name, drug_link.url
        )
        side_effects = self.parse_side_effects(
            drug_page, drug_link.drug_name, drug_link.url
        )
        contra_indications = self.parse_contra_indications(
            drug_page, drug_link.drug_name, drug_link.url
        )
        cautions = self.parse_cautions(
            drug_page, drug_link.drug_name, drug_link.url
        )
        return Drug(
            drug_link.drug_name,
            drug_link=drug_link,
            components=components,
            side_effects=side_effects,
            indications=indications,
            contra_indications=contra_indications,
            cautions=cautions,
            synonyms=synonyms
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def clean_drug_name(self, treatment_name):
        """Clean unwanted text from the drug name.

        Parameters
        ----------
        treatment_name : `str`
            The name of the treatment.

        Returns
        -------
        treatment_name : `str`
            The cleaned name of the treatment.
        """
        treatment_name = treatment_name.strip()
        return re.sub(
            r'\s+\[specialist drug\]$', '', treatment_name, flags=re.IGNORECASE
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def clean_component_name(self, component_name):
        """Clean unwanted text from the drug component name.

        Parameters
        ----------
        component_name : `str`
            The name of the drug component.

        Returns
        -------
        component_name : `str`
            The cleaned name of the drug component.
        """
        component_name = component_name.strip()
        component_name = re.sub(r'\(\w+\)$', '', component_name.strip())
        component_name = re.sub(
            r'\s+\[specialist drug\]$', '', component_name,
            flags=re.IGNORECASE
        )
        component_name = re.sub(
            r'^dried$', '', component_name, flags=re.IGNORECASE
        )
        component_name = re.sub(
            r'(solution|fraction|concentrate)$',
            '',
            component_name,
            flags=re.IGNORECASE
        )
        return component_name.strip()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_drug_components(self, treatment_name):
        """Parse the treatment into it's formulation components.

        Parameters
        ----------
        treatment_name : `str`
            The name of the treatment.

        Returns
        -------
        components : `list` of `str`
            The components of the drug.
        """
        is_vaccine = False
        is_vitamin = False
        if re.search(r'vaccine', treatment_name, re.IGNORECASE):
            is_vaccine = True
        elif re.search(r'vitamin', treatment_name, re.IGNORECASE):
            is_vitamin = True

        # in the BNF to get the formation components we split on WITH AND and ,
        components = re.split(
            r'\bWITH\b|\bAND\b|,|/', treatment_name, flags=re.IGNORECASE
        )

        clean_components = []
        for idx, i in enumerate(components):
            i = self.clean_component_name(i)
            if i != '':
                clean_components.append(i)
        components = clean_components

        if is_vaccine is True:
            org_regexp = re.search(r'(\w+(?:\s+\w+)?)\s+\w{1}$', components[0])

            try:
                org = org_regexp.group(1)
                org = f'{org} '
            except AttributeError:
                org = ''

            for idx, i in enumerate(components):
                if re.match(r'^(\w{1}|\w\d+)\b', i):
                    i = f'{org}{i}'
                if not re.search(r'vaccine(s)?$', i, re.IGNORECASE):
                    i = f'{i} vaccine'
                i = re.sub(r'vaccines', 'vaccine', i, flags=re.IGNORECASE)
                i = re.sub(r'groups', 'group', i, flags=re.IGNORECASE)
                components[idx] = i
        elif is_vitamin is True:
            for idx, i in enumerate(components):
                if re.match(r'^\w{1}$', i):
                    i = f'vitamin {i}'
                i = re.sub(r'vitamins', 'vitamin', i, flags=re.IGNORECASE)
                components[idx] = i

        return components

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_indication_roa(self, treat_page, drug_name, drug_url):
        """Parse out the indication, ROA and dosages fields. The dosage and ROA
        is tied to a specific indication.

        Parameters
        ----------
        treat_page : `lxml.html.HtmlElement`
            The drug treatment webpage to search in.
        drug_name : `bnf_download.bnf.DrugLink`
            The treatment name and location of the treatment webpage.

        Returns
        -------
        indications : `dict`
            The keys are the indication string the values are a dict of ROA as
            keys and a values are further dict with patient group (i.e. Adult)
            as keys and the dosage description as values.
        synonyms : `list` of `tuple`
            Any drug synonym names that are found when parsing the indication.
            Examples of synonyms are drug brand names. Each tuple has:

            0. The drug name as described in the BNF.
            1. The synonym name - i.e. a brand name.
            2. A boolean indicating if the synonym is a brand name.

            This list will have the minimum length 1, even if no synonyms are
            found this will have a tuple with the drug_name/synonym name will
            be identical with the brand boolean being False.
        """
        # Will hold the extracted indications
        indications = dict()

        synonyms = set()
        synonyms.add((drug_name, drug_name, False))

        # The xpath for the indications and ROA contains the drug name in lower
        # case with and non word characters removed and concatenated with -
        # xpath_drug_name = self._link_drug_name(drug_name)
        indication_section_xpath = (
            './/section[@aria-labelledby="indications-and-dose"]'
        )

        indication_section = treat_page.xpath(indication_section_xpath)
        if len(indication_section) > 1:
            raise IndexError(
                "Expected a single indication section not: "
                f"{len(indication_section)} for {drug_name} ({drug_url})"
            )
        elif len(indication_section) == 0:
            return indications, list(synonyms)
        indication_section = indication_section[0]
        summary_section = indication_section.xpath('./section/details')
        if len(summary_section) > 0:
            for i in summary_section:
                drug_ind_name = re.sub(
                    r'-indications-and-dose$', '',
                    i.getparent().get('aria-labelledby')
                )
                drug_brand_name = re.sub(
                    r'[Ff]or\s+', '',
                    " ".join(i.xpath('./summary/h3/text()'))
                )
                is_brand = drug_brand_name.endswith('®')
                synonyms.add(
                    (drug_name, drug_ind_name, is_brand)
                )

                ind_xpath = (
                    './section[starts-with(@aria-labelledby, '
                    f'"{drug_ind_name}-indication-")]'
                )
                for g in i.xpath(ind_xpath):
                    roa_dose = self.parse_roa_dosage(g)
                    for x in self.parse_indications(g):
                        indications.setdefault(x, dict())
                        indications[x] = {**indications[x], **roa_dose}
        else:
            ind_xpath = (
                './section[contains(@aria-labelledby, "-indications-and-dose")]'
                '/section[starts-with(@class, '
                '"IndicationsAndDoseContent-module-")]'
            )
            # This will loop through different indication groups, each group
            # has an indication string and one or more ROAs which can contain 1
            # or more patient groups
            for g in indication_section.xpath(ind_xpath):
                drug_ind_name = re.sub(
                    r'-indication-\d+', '',
                    g.get("aria-labelledby")
                )
                drug_ind_name = re.sub(r'-+', ' ', drug_ind_name)
                is_brand = False
                if drug_name.lower() != drug_ind_name.lower():
                    is_brand = True
                synonyms.add((drug_name, drug_ind_name, is_brand))
                roa_dose = self.parse_roa_dosage(g)
                for x in self.parse_indications(g):
                    indications.setdefault(x, dict())
                    indications[x] = {**indications[x], **roa_dose}

        # So I have noticed that some of the drugs link their indications to
        # other drugs through hrefs and acton="load" commands. So this just
        # makes sure that there are no other indications buried in there
        # Make sure there are no action=load links in there for other
        # indications
        # section = '#indicationsAndDoses'
        # check_url = drug_url
        # if not check_url.endswith(section):
        #     check_url = '{0}{1}'.format(drug_url, section)
        # # print(check_url)
        # for i in treat_page.iterlinks():
        #     # i is a tuple of <ELEMENT>, str link name (i.e. href), URL>
        #     # if the URL endswith the indication section and the URL is not
        #     # the one for the current drug, then we act on it
        #     if i[2].endswith(section) and \
        #        i[2] != check_url:
        #         # print(i)
        #         r = requests.get(i[2])
        #         tree = html.fromstring(r.content)
        #         tree.make_links_absolute(r.url)
        #         indications = {
        #             **indications,
        #             **self.parse_indication_roa(tree, drug_name, r.url)
        #         }

        return indications, list(synonyms)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_indications(self, indication_section):
        """Parse the indication string from an indication section.

        Parameters
        ----------
        indication_section : `lxml.html.HtmlElement`
            A single indication section, each group has an indication string
            and one or more ROAs which can contain 1 or more patient groups.
            However, this function only extracts the indication string.

        Returns
        -------
        indications : `list` of `str`
            Indications extracted from the section. Even though a list is
            returned we really only expect one.
        """
        indication_text_xpath = (
            './h4/span[starts-with(@class, '
            '"IndicationsAndDoseContent-module--indicationText")'
            ']/text()|./h3/span[starts-with(@class, '
            '"IndicationsAndDoseContent-module--indicationText")'
            ']/text()'
        )
        indications = []
        for i in indication_section.xpath(indication_text_xpath):
            try:
                # Remove any nowlines from the effect string, if it is
                # empty this will error and we will move on
                i = self._clean_string(i)
                indications.append(i)
            except ValueError:
                continue
        return indications

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_roa_dosage(self, indication_section):
        """Parse the ROA and patient group and dosage from an indication
        section.

        Parameters
        ----------
        indication_section : `lxml.html.HtmlElement`
            A single indication section, each group has an indication string
            and one or more ROAs which can contain 1 or more patient groups.
            However, this function only extracts the ROA, patient group and
            dosage information.

        Returns
        -------
        roa_dose : `dict` of `dict` of `str`
            The keys of the outer dict are ROA strings, the keys of the inner
            dict are patient groups (i.e. Adult) and the values are dosage
            strings.
        """
        roa_section_xpath = (
            './section[starts-with(@class, '
            '"IndicationsAndDoseContent-module--routeOfAdministration")]'
        )
        roa_xpath = ('./h5/text()')
        dose_section_xpath = (
            './dl/div[starts-with(@class, "IndicationsAndDoseContent-module'
            '--patientGroupDose")]'
        )
        roa_dose = dict()
        for i in indication_section.xpath(roa_section_xpath):
            for r in i.xpath(roa_xpath):
                r = self._clean_string(r)
                roa_dose.setdefault(r, dict())

                for d in i.xpath(dose_section_xpath):
                    dose_patient = self._clean_string(
                        d.xpath('./dt/text()')
                    )
                    dose_desc = self._clean_string(
                        d.xpath('./dd/text()')
                    )
                    roa_dose[r][dose_patient] = dose_desc
        return roa_dose

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_side_effects(self, treat_page, drug_name, drug_url):
        """Parse out the side effect fields.

        Parameters
        ----------
        treat_page : `lxml.html.HtmlElement`
            The drug treatment webpage to search in.
        drug_name : `str`
            The name of the drug.
        drug_url : `str`
            The URL of the drug page.

        Returns
        -------
        side_effects : `list` of (`str`, `str`, `bool`, (`str` or `NoneType`))
            The side effects for the drug, each entry in the list is a specific
            side effect. The data in the tuple is (qualitative_frequency,
            side_effect, is_drug_class_effect, drug_class_name).
        """
        side_effects = set()
        xpath_drug_name = self._link_drug_name(drug_name)
        drug_se_match = f"side-effects-{xpath_drug_name}"
        se_section_xpath = (
            '//section[@aria-labelledby="side-effects"]'
            '/section[starts-with(@aria-labelledby, "side-effects-")]'
        )
        for i in treat_page.xpath(se_section_xpath):
            class_effect = False
            drug_class = None
            if i.get('aria-labelledby') != drug_se_match:
                class_effect = True
                drug_class = re.sub(
                    r'-', ' ',
                    re.sub(
                        r"side-effects-", '', i.get('aria-labelledby')
                    )
                )
            side_effects.update(
                self._parse_side_effects(
                    i, drug_name, drug_class_effect=class_effect,
                    drug_class=drug_class
                )
            )
        # This old code I do not think is relevant anymore but I have left
        # it here just in case.
        # SECTION = '#sideEffects'
        # for se in treat_page.xpath('//section[@id="sideEffects"]'):
        #     drug_class = se.xpath(
        #         "//h3[contains(text(),'For all')]/span/text()")
        #     # etree.tostring(se, encoding='unicode', pretty_print=True)
        #     for i in se.iterlinks():
        #         if i[2].endswith(SECTION):
        #             r = requests.get(i[2])
        #             tree = html.fromstring(r.content)
        #             # print(i[2])
        #             # print(etree.tostring(tree, encoding='unicode',
        #             #                      pretty_print=True))
        #             tree.make_links_absolute(r.url)
        #             side_effects = side_effects.union(
        #                 self._parse_side_effects(
        #                     tree,
        #                     drug_class_effect=True,
        #                     drug_class=drug_class[0]))
        return list(side_effects)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_side_effects(self, se_section, drug_name,
                            drug_class_effect=False, drug_class=None):
        """Parse out the side effects.

        Parameters
        ----------
        se_section : `lxml.html.HtmlElement`
            The side effect section of the webpage to search in.
        drug_class_effect : `bool`, optional, default: `False`
            The if the side effect a drug class effect.
        drug_class : `str` or `NoneType`
            The drug class if ``drug_class_effect`` is not ``NoneType``.

        Returns
        -------
        side_effects : `list` of (`str`, `str`, `bool`, (`str` or `NoneType`))
            The side effects for the drug, each entry in the list is a specific
            side effect. The data in the tuple is (qualitative_frequency,
            side_effect, is_drug_class_effect, drug_class_name).
        """
        side_effects = set()
        drug_link = ""
        if drug_class_effect is False:
            drug_link = self._link_drug_name(drug_name)
        else:
            drug_link = self._link_drug_name(drug_class)

        test_section_name = se_section.get('aria-labelledby')
        if test_section_name != f'side-effects-{drug_link}':
            raise KeyError(
                "unexpected side effects section:"
                f" {drug_name} - {test_section_name}"
            )
        # pp.pprint(se_section.get('aria-labelledby'))
        # pp.pprint([i.tag for i in se_section.iter()])
        se_data_xpath = './div/h3|./div/h4|./div/p'
        # se_values = se_section.xpath('./div/p')

        se_key = None
        for i in se_section.xpath(se_data_xpath):
            if i.tag in ['h4', 'h3']:
                se_key = i.text
                if se_key == "Side-effects, further information":
                    se_key = 'info'
                se_key = re.sub(r'[\W\s]+', '_', se_key.lower())
            else:
                if se_key is None:
                    raise KeyError(
                        f"problem parsing side-effects: {drug_name}"
                    )

                if se_key not in ['info', 'overdose']:
                    se = [x.strip() for x in self._extract_text(i).split(";")]
                else:
                    se = [self._extract_text(i).strip()]
                # else:
                #     se = " ".join([ifor i in se])
                for j in se:
                    side_effects.add(
                        (
                            se_key, re.sub(r'\s+', ' ', j), drug_class_effect,
                            drug_class
                        )
                    )

        return side_effects

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_contra_indications(self, treat_page, drug_name, drug_url):
        """Parse out the contra-indication fields.

        Parameters
        ----------
        treat_page : `lxml.html.HtmlElement`
            The drug treatment webpage to search in.
        drug_name : `str`
            The name of the drug.
        drug_url : `str`
            The URL of the drug page.

        Returns
        -------
        contra_indications : `list` of `str`
            The contra-indications.
        """
        xpath_drug_name = self._link_drug_name(drug_name)
        ci_xpath = (
            '//section[@aria-labelledby="contra-indications"]'
            '/section[@aria-labelledby="contra-indications-'
            f'{xpath_drug_name}"]/div'
        )
        ci = set()
        for i in treat_page.xpath(ci_xpath):
            # print(i.text_content())
            ci.update([x.strip() for x in self._extract_text(i).split(";")])
        # pp.pprint(ci)
        return list(ci)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_cautions(self, treat_page, drug_name, drug_url):
        """Parse out the caution fields.

        Parameters
        ----------
        treat_page : `lxml.html.HtmlElement`
            The drug treatment webpage to search in.
        drug_name : `str`
            The name of the drug.
        drug_url : `str`
            The URL of the drug page.

        Returns
        -------
        cautions : `list` of `str`
            The cautions.
        """
        xpath_drug_name = self._link_drug_name(drug_name)
        ci_xpath = (
            '//section[@aria-labelledby="cautions"]'
            '/section[@aria-labelledby="cautions-'
            f'{xpath_drug_name}"]/div/p'
        )
        ci = set()
        for i in treat_page.xpath(ci_xpath):
            try:
                ci.update(
                    [x.strip() for x in self._extract_text(i).split(";")]
                )
            except AttributeError:
                print(drug_name)
                if i.text is not None:
                    raise
        return list(ci)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _link_drug_name(drug_name):
        """Process the drug name for use in an xpath query.

        Parameters
        ----------
        drug_name : `str`
            The drug name to process.

        Returns
        -------
        processed_drug_name : `str`
            The processed drug name for use in an xpath query.

        Notes
        -----
        Processing involved making the drug name lower case and removing non
        word characters before substituting spaces with concatenated
        with ``-``.
        """
        xpath_drug_name = re.sub(r'\W+', ' ', drug_name.lower())
        return re.sub(r'\s+', '-', xpath_drug_name.strip())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _extract_text(self, element):
        """Extract text from an HTML element. This is intended to deal with
        nested HTML tags such as superscript (sup), strong.

        Parameters
        ----------
        element : `lxml.html.HtmlElement`
            The HTML element containing text.

        Notes
        -----
        Superscripts are processed into ``text^superscript_text``.
        """
        text = ""
        for i in element.xpath('./p/text()|.//sup|.//strong|./text()'):
            try:
                tag_text = i.text
                if i.tag == 'sup':
                    text = f"{text}^{tag_text}"
                else:
                    text = f"{text} {tag_text}"
            except AttributeError:
                text = f"{text} {i}"
        return self._clean_string(text)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _clean_string(self, effect_string):
        """Clean up an extracted string from the HTML, raise an error if the
        string is empty after processing.

        Parameters
        ----------
        effect_string : `str` or `list` of `str`
            A string or a list of strings, lists are concatenated on a space.

        Returns
        -------
        clean_string : `str`
            A cleaned string, this will have any newlines removed and any
            unicode spaces replaced with ascii spaces.

        Raises
        ------
        ValueError
            If the string is empty after processing.
        """
        if isinstance(effect_string, list):
            effect_string = " ".join(effect_string)
        effect_string = re.sub(r'[\r\n]', '', effect_string)

        effect_string = effect_string.replace('\xa0', ' ')
        # effect_string = unicodedata.normalize('NFD', effect_string)
        if re.match(r'^[\s\t]*$]', effect_string):
            raise ValueError("effect string is null")

        return effect_string


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugLink(object):
    """Representation of a URL linking to a drug record.

    Parameters
    ----------
    drug_name : `str`
        The name of the drug.
    url : `str`
        The URL where all the drug data will reside.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, drug_name, url):
        self.drug_name = drug_name
        self.url = url

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        return "<{0}('{1}','{2}')>".format(
            self.__class__.__name__,
            self.drug_name,
            self.url
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_drug_page(self):
        """Navigate to the BNF drug page.

        Returns
        -------
        drug_html : `lxml.html.HtmlElement`
            The HTML containing the drug data.

        Raises
        ------
        RequestsError
            If the web request gives a bad response code.
        """
        r = requests.get(self.url)
        r.raise_for_status()

        # Now parse the drugs out of the page
        tree = html.fromstring(r.content)
        tree.make_links_absolute(r.url)
        return tree

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def print_drug_page(self):
        """Print the HTML of the page.

        Raises
        ------
        RequestsError
            If the web request gives a bad response code.
        """
        r = requests.get(self.url)
        r.raise_for_status()

        # Now parse the drugs out of the page
        tree = html.fromstring(r.content)
        tree.make_links_absolute(r.url)
        print(etree.tostring(tree, encoding='unicode', pretty_print=True))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open_drug_page(self):
        """Navigate to the drug data page.

        Raises
        ------
        RequestsError
            If the web request gives a bad response code.
        """

        r = requests.get(self.url)
        r.raise_for_status()

        # Now parse the drugs out of the page
        tree = html.fromstring(r.content)
        html.open_in_browser(tree)
        webbrowser.open_new(self.url)
        # tree.make_links_absolute(r.url)

        # print(etree.tostring(tree, encoding='unicode', pretty_print=True))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Drug(object):
    """Representation of a processed drug entry.

    Parameters
    ----------
    drug_list :`bnf_download.bnf.DrugLink`
        The name of the drug and it's base URL containing the drug data.
    components : `list` of `str`
        The individual components of the drug.
    indications : `dict`
        The keys are the indication string the values are a dict of ROA as
        keys and a values are further dict with patient group (i.e. Adult)
        as keys and the dosage description as values.
    side_effects : `list` of (`str`, `str`, `bool`, (`str` or `NoneType`))
        The side effects for the drug, each entry in the list is a specific
        side effect. The data in the tuple is (qualitative_frequency,
        side_effect, is_drug_class_effect, drug_class_name).
    contra_indications : `list` of `str`
        Any contra-indications for a drug.
    cautions : `list` of `str`
        Any cautions for a drug.
    synonyms : `list` of `str`
        Any synonyms/brand names for a drug.
    """
    _EXPECTED_ARGS = [
        ('drug_link', DrugLink),
        ('components', list),
        ('indications', dict),
        ('side_effects', list),
        ('contra_indications', list),
        ('cautions', list),
        ('synonyms', list)
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, drug_name, **kwargs):
        self.drug_name = drug_name

        for i in self.__class__._EXPECTED_ARGS:
            try:
                arg = kwargs[i[0]]

                if not isinstance(arg, (i[1])):
                    raise TypeError(
                        "expected '{0}' for '{1}'".format(
                            type(i[1]), i[0]
                        )
                    )
            except KeyError:
                arg = None

            # Set the attribute
            setattr(self, i[0], arg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Nice representation.
        """
        return utils.obj_repr(self)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_request_response(response):
    """Tests the response from an web request and give an error if it is not in
    the 200 range

    Parameters
    ----------
    response : `response`
        A requests response from a get.

    Raises
    ------
    RuntimeError
        If the response does not start with 2
    """

    # If it does nto satrtwith 2 then error out
    if not str(response.status_code).startswith("2"):
        raise RuntimeError("Error status_code of '{0}'".format(
            response.status_code))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class BnfEffectMapper(object):
#     """
#     Map the drug effects to UMLS terms (where possible)
#     """

#     METAMAP_ARGS = ['-L', '2018AA', '-Z', '2018AA', '-sAIy', '--negex',
#                     '--conj', '-V', 'USAbase']

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __init__(self, metamap):
#         """
#         initialise

#         Parameters
#         ----------

#         metamap : :obj:`MetamapServer`
#             The instance linking to the server to run metamap queries
#         """
#         self.metamap = metamap

#         self.with_leads = {}
#         self.with_lags = {}

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def parse(self, effect_phrase):
#         """
#         Parse an effect phrase

#         Parameters
#         ----------

#         effect_phrase : :obj:`str`
#             The effect phase to map to the UMLS

#         Returns
#         -------

#         effect_mapping :`dict`
#             The effect mappings and any prerequisite codependencies
#         """
#         original_phrase = effect_phrase
#         print("*****")
#         print(original_phrase)

#         # Determine if the effect (indication) should only be used by an expert
#         expert, effect_phrase = self.is_expert(effect_phrase)
#         diag, effect_phrase = self.is_diagnostinc(effect_phrase)

#         # Run metamap on the phrase
#         results = self.get_metamap_mappings(effect_phrase)
#         # pp.pprint(results)
#         phrases = self.split_on_with(effect_phrase)
#         pp.pprint(phrases)
#         # Now we place the metamap results in context with the phrase to
#         # determine the indications and preconditions
#         # tags = self.get_tags(results)
#         # pp.pprint(tags)
#         # # These might have some bearing on the interpretation
#         flanking_words = self.flanking_words(phrases)

#         try:
#             self.with_leads.setdefault(flanking_words[0][1], 0)
#             self.with_lags.setdefault(flanking_words[1][0], 0)

#             # increament our counter keeping track of what occrs either side of a
#             # "with" effect
#             self.with_leads[flanking_words[0][1]] += 1
#             self.with_lags[flanking_words[1][0]] += 1
#         except IndexError:
#             # No WITH
#             pass

#         # Now split on any and/or groups
#         for counter, phrase in enumerate(phrases):
#             phrases[counter] = self.split_on_and_or(phrase)

#         print("*** ORIGINAL_PHRASE: {0}".format(original_phrase))
#         pp.pprint(phrases)
#         print("")
#         if diag is True:
#             pp.pprint([original_phrase] + phrases + [expert, diag])
#             pp.pprint(flanking_words)
#             print("")

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_tags(self, results):
#         """
#         Add the metamap mappings to the term text to overwrite the text and
#         replace it with metamap tags 

#         Parameters
#         ----------

#         results : :obj:`list` of `Utterance`
#             The metamap results, there is probably only one Utterance but in
#             theory there could be more
#         """
#         tags = []

#         excludes = ['Functional Concept', 'Qualitative Concept',
#                     'Quantitative Concept']
#         mapping = {'Disease or Syndrome': 'DISEASE',
#                    'Pathologic Function': 'DISEASE',
#                    'Patient or Disabled Group': 'PATIENT',
#                    'Pharmacologic Substance': 'INTERVENTION',
#                    'Organic Chemical': 'INTERVENTION'}

#         # Loop through all the documents
#         for doc in results:
#             # Loop through all the utterances
#             for utt in doc:
#                 # Now generate a TagTerm object to overlay the metamap tags
#                 # onto the text
#                 tt = metamap.TagTerm(utt.text, exclude=excludes,
#                                      remap=mapping)
#                 # pp.pprint(tt.proc_term)
#                 tt.add_utterance(utt)
#                 # pp.pprint(tt.candidates)
#                 # pp.pprint(tt.get_tag_list())
#                 tt.add_tag_equals_str('with', 'WITH')
#                 tt.add_tag_equals_str('and', 'AND')
#                 tt.add_tag_equals_str('or', 'AND')
#                 tt.add_tag_equals_str('treatment', 'TREAT')
#                 tt.add_tag_equals_str('treat', 'TREAT')
#                 tt.add_tag_equals_str('prophalaxis', 'TREAT')
#                 tt.add_tag_equals_str('not', 'NOT')

#                 # pp.pprint(tt.get_tag_list())
#                 print("* {0}".format(tt))
#                 tags.append(tt)

#         return tags

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_metamap_mappings(self, effect_phrase):
#         """

#         """
#         return self.metamap.run(effect_phrase, self.__class__.METAMAP_ARGS)
#         # flat_results = metamap.summarise(results)
#         # pp.pprint(flat_results)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def is_expert(self, effect_phrase):
#         """
#         Determine if the effect should be deliverable by an expert

#         Parameters
#         ----------

#         effect_phrase : :obj:`str`
#             The effect phase to map to the UMLS

#         Returns
#         -------

#         expert :`tuple`
#             element [0] is a bool, if True then the effect is an "expert"
#             effect. element [1] is the effect_phrase but with any expert flags
#             removed
#         """
#         # Substitute any specialist bracketed terms
#         effect, subs = re.subn(r'\([\w\s]*(specialist|expert)[\w\s]*\)', '',
#                                effect_phrase)

#         # Remove any trailing whitespace
#         effect = effect.strip()

#         if subs > 0:
#             return True, effect

#         return False, effect

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def is_diagnostinc(self, effect_phrase):
#         """
#         Determine if the drug effect is used for diagnosis
#         """

#         # aid to diagnonsis, this will be removed if present
#         effect_phrase, subs = re.subn(r'aid\s+to\s+diagnosis', '',
#                                       effect_phrase, flags=re.IGNORECASE)
#         effect_phrase = effect_phrase.strip()

#         # Does imaging appear
#         # if re.search(r'imaging', effect_phrase):
#         #     subs += 1

#         if subs > 0:
#             return True, effect_phrase

#         return False, effect_phrase

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def split_on_with(self, effect_phrase):
#         """
#         Split the effect_phrase on the first occurence of "with"

#         Parameters
#         ----------

#         effect_phrase : :obj:`str`
#             The effect phase to map to the UMLS
#         """

#         return [i.strip() for i in re.split(r'\bwith\b', effect_phrase,
#                                             maxsplit=1, flags=re.IGNORECASE)]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def split_on_and_or(self, phrase):
#         """
#         Split the effect_phrase on any occurence of and/or

#         Parameters
#         ----------

#         effect_phrase : :obj:`str`
#             The effect phase to map to the UMLS
#         """

#         return [i.strip() for i in re.split(r'\b(?:and|or)\b', phrase,
#                                             flags=re.IGNORECASE)]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def flanking_words(self, phrases):
#         """
#         get the words flanking the prases
#         """
#         flanks = []

#         for i in phrases:
#             lead_re = re.match(r'^\b(\w+)\b', i)
#             lag_re = re.search(r'\b(\w+)\b$', i)

#             try:
#                 lead_word = lead_re.group(1)
#             except AttributeError:
#                 lead_word = None

#             try:
#                 lag_word = lag_re.group(1)
#             except AttributeError:
#                 lag_word = None

#             flanks.append((lead_word, lag_word))

#         return flanks
