"""Whilst this module is called ORM, it is actually a collection of queries put
 together in a very non-SQlalchemy way. Apologies, I was in a rush putting this
 together, I will refactor all this at some point!
"""
from sqlalchemy import text
from warnings import warn
import re
import pprint as pp

warn(
    "bnf_download.chembl_orm is deprecated please use dedicated "
    "chembl-orm package: https://gitlab.com/cfinan/chembl-orm",
     DeprecationWarning, 2
)

# TODO: This was put together in a hurry, turn raw queries into SQLAlchemy

# Used in the mapping of drug names to chembl IDs
END_SUBS = [re.compile(r'\s+solution$', re.IGNORECASE),
            re.compile(r'\s+\w*acetate$', re.IGNORECASE),
            re.compile(r'\s+\w*hydrate$', re.IGNORECASE),
            re.compile(r'\s+\w*oxide$', re.IGNORECASE),
            re.compile(r'\s+extract$', re.IGNORECASE),
            re.compile(r'\s+pivalate$', re.IGNORECASE),
            re.compile(r'\s+\w*sulfate$', re.IGNORECASE),
            re.compile(r'\s+\w*sulphate$', re.IGNORECASE),
            re.compile(r'\s+tartrate$', re.IGNORECASE),
            re.compile(r'\s+citrate$', re.IGNORECASE),
            re.compile(r'\s+decanoate$', re.IGNORECASE),
            re.compile(r'\s+arginate$', re.IGNORECASE),
            re.compile(r'\s+\w*phosphate$', re.IGNORECASE),
            re.compile(r'\s+\w*oxalate$', re.IGNORECASE),
            re.compile(r'\s+\w*hydrochloride$', re.IGNORECASE),
            re.compile(r'\s+embonate$', re.IGNORECASE)]

START_SUBS = [re.compile(r'^co-', re.IGNORECASE),
              re.compile(r'-')]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_chembl_id_for_uniprot(session, uniprot):
    """Get all the chembl target IDs for a uniprot accession, currently the
    queries are text and optimised for SQLite.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database
    uniprot : `str`
        A uniprot identifier

    Returns
    -------
    chembl_ids :`list` of `str`
        The chembl identifiers mapping to the uniprot ID. If no mapping exists
        this will be an empty string.
    """
    sql = text(
        """
        select chembl_id
        from component_sequences cs
        join target_components tc
        on tc.component_id = cs.component_id
        join target_dictionary td on tc.tid = td.tid
        where accession = :UNIPROT
        """
    )
    query = session.execute(sql, {'UNIPROT': uniprot})
    return [i.chembl_id for i in query.fetchall()]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_summary(session, chembl_id):
    """Get a condensed summary of the target fromm ChEMBL.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database
    chembl_id : `str`
        A chembl identifier for the target.

    Returns
    -------
    chembl_id : `str`
        The chembl identifiers (same as was passed)
    target_type : `str`
        The type of the target, i.e. ``SINGLE_PROTEIN``, ``PROTEIN_COMPLEX``.
    pref_name : `str`
        The preferred name of the target.
    n_components : `int`
        The number of components in the target.
    target_components : `str`
        A ``,`` delimited sting of the uniprot IDs that make up the target
    """
    sql = text(
        """
        select td.chembl_id,
               td.target_type,
               td.pref_name,
               count(*) as n_components,
               group_concat(cs.accession) as components
        from component_sequences cs
        join target_components tc
        on tc.component_id = cs.component_id
        join target_dictionary td on tc.tid = td.tid
        where td.chembl_id = :CHEMBL_ID
        group by td.chembl_id
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    try:
        return query.fetchall()[0]
    except IndexError:
        return (None, None, None, None, None)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_target_indications(session, chembl_id):
    """
    """
    sql = text(
        """
        select td.chembl_id as target_chembl_id,
        td.pref_name as target_pref_name,
        md.chembl_id as compound_chembl_id,
        md.pref_name as compound_pref_name,
        dm.mechanism_of_action,
        dm.action_type,
        dm.direct_interaction,
        dm.molecular_mechanism,
        dm.disease_efficacy,
        dm.mechanism_comment,
        dm.selectivity_comment,
        dm.binding_site_comment,
        di.max_phase_for_ind,
        di.mesh_id,
        di.mesh_heading,
        di.efo_id,
        di.efo_term
        from target_dictionary td
        join drug_mechanism dm on td.tid = dm.tid
        join molecule_dictionary md on dm.molregno = md.molregno
        left join drug_indication di on dm.molregno = di.molregno
        where td.chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_compound_summary(session, chembl_id):
    """
    """
    sql = text(
        """
        select  md.chembl_id
        , md.pref_name
        , md.indication_class
        , md.max_phase
        , md.therapeutic_flag
        , md.molecule_type
        , md.first_approval
        , md.black_box_warning
        , md.withdrawn_flag
        , md.oral
        , md.parenteral
        , md.topical
        , cp.num_lipinski_ro5_violations
        , cp.ro3_pass
        , md.usan_year
        from molecule_dictionary md
        left join compound_properties cp
        on md.molregno = cp.molregno
        where chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    res = query.fetchall()
    if len(res) > 1:
        pp.pprint(res)
        raise IndexError("too many results")
    return res[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_compound_atc(session, chembl_id):
    """
    """
    sql = text(
        """
        select  md.chembl_id
        , ac.who_name
        , mac.level5
        , ac.level1_description
        , ac.level2_description
        , ac.level3_description
        , ac.level4_description
        from molecule_dictionary md
        join molecule_atc_classification mac
        on md.molregno = mac.molregno
        join atc_classification ac
        on mac.level5 = ac.level5
        where chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_compound_synonyms(session, chembl_id):
    """
    """
    sql = text(
        """
        select  md.chembl_id, ms.*
        from molecule_dictionary md
        join molecule_synonyms ms
        on ms.molregno = md.molregno
        where chembl_id = :CHEMBL_ID
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_activity_data(session, chembl_id):
    """Get the activity data for a chembl target.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQL alchemy session object to interact with the ChEMBL database
    chembl_id : `str`
        A chembl identifier for the target.

    Returns
    -------
    activity_data
    """
    sql = text(
        """
        select  td.chembl_id as "target_chembl_id"
        , md.chembl_id as "compound_chembl_id"
        , md.pref_name
        , md.max_phase
        , md.molecule_type
        , md.indication_class
        , s.chembl_id as assay_chembl_id
        , s.description as assay_description
        , ast.*
        , s.assay_test_type
        , s.assay_category
        , s.assay_organism
        , s.assay_strain
        , s.assay_tissue
        , s.assay_cell_type
        , rt.*
        , s.confidence_score
        , s.curated_by
        , a.standard_type
        , a.standard_relation
        , a.standard_value
        , a.standard_units
        , a.standard_flag
        , a.pchembl_value
        , a.activity_comment
        , a.standard_text_value
        from molecule_dictionary md
        join activities a
        on a.molregno = md.molregno
        join assays s
        on s.assay_id = a.assay_id
        join relationship_type rt
        on rt.relationship_type = s.relationship_type
        join assay_type ast
        on ast.assay_type = s.assay_type
        join target_dictionary td
        on td.tid = s.tid
        where td.chembl_id = :CHEMBL_ID and s.assay_type != "A"
        """
    )
    query = session.execute(sql, {'CHEMBL_ID': chembl_id})
    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_activity_data_for_uniprot(session, uniprot_accession):
    """
    Get all the compounds that have an activity measure against the uniprot_id

    Parameters
    ----------

    session : :obj:`Session`
        The SQL alchemy session
    chembl_id :`str`
        The chembl ID to get the efficacy targets for
    """

    sql = text("""select md.pref_name as compound_pref_name
    , md.chembl_id as "compound_chembl_id"
    , md.max_phase
    , md.molecule_type
    , md.indication_class
    , td.chembl_id as "target_chembl_id"
    , td.pref_name as target_pref_name
    , td.target_type
    , td.organism
    , cs.component_type
    , cs.accession as uniprot_accession
    , cs.description as uniprot_description
    , s.chembl_id as assay_chembl_id
    , s.description as assay_description
    , ast.*
    , s.assay_test_type
    , s.assay_category
    , s.assay_organism
    , s.assay_strain
    , s.assay_tissue
    , s.assay_cell_type
    , rt.*
    , s.confidence_score
    , s.curated_by
    , a.standard_type
    , a.standard_relation
    , a.standard_value
    , a.standard_units
    , a.standard_flag
    , a.pchembl_value
    , a.activity_comment
    , a.standard_text_value
    from molecule_dictionary md
    join activities a
    on a.molregno = md.molregno
    join assays s
    on s.assay_id = a.assay_id
    join relationship_type rt
    on rt.relationship_type = s.relationship_type
    join assay_type ast
    on ast.assay_type = s.assay_type
    join target_dictionary td
    on td.tid = s.tid
    join target_components tc
    on tc.tid = s.tid
    join component_sequences cs
    on cs.component_id = tc.component_id
    where cs.accession = :UNIPROT and s.assay_type != "A"
    """)

    query = session.execute(sql, {'UNIPROT': uniprot_accession})

    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_drug_targets(session, chembl_id):
    """
    Get all the drug target components for a chembl ID

    Parameters
    ----------

    session : :obj:`Session`
        The SQL alchemy session
    chembl_id :`str`
        The chembl ID to get the efficacy targets for
    """

    sql = text("""select md.pref_name as compound_pref_name
    , md.chembl_id as "compound_chembl_id"
    , molecule_type
    , indication_class
    , mechanism_of_action
    , td.chembl_id as "target_chembl_id"
    , td.target_type
    , td.pref_name as "target_pref_name"
    , td.organism
    , action_type
    , accession as "uniprot_accession"
    , cs.description as "uniprot_description"
    from molecule_dictionary md
    join drug_mechanism dm
    on dm.molregno = md.molregno
    join target_dictionary td
    on td.tid = dm.tid
    join target_components tc
    on tc.tid = dm.tid
    join component_sequences cs
    on cs.component_id = tc.component_id
    where md.chembl_id = :DRUG
    group by md.chembl_id, td.chembl_id, cs.accession
    """)
    query = session.execute(sql, {'DRUG': chembl_id})

    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_drug_targets(session, chembl_id):
    """
    Attempt to get a drug target for a chembl ID. The difference between this
    and get_drug_targets is that if no drug target is found then we check for
    any drug targets for the parent molecule or the active molecule, this uses
    the chembl molecule_hierarchy table

    Parameters
    ----------

    session : :obj:`Session`
        The SQL alchemy session
    chembl_id :`str`
        The chembl ID to get the efficacy targets for

    """

    # First attempt to find drug targets for the chembl_id
    drug_targets = get_drug_targets(session, chembl_id)

    # if we have found some then we return them
    if len(drug_targets) > 0:
        drug_targets = [(i, chembl_id, 'direct') for i in drug_targets]
        return drug_targets

    # If we get here then no drug targets have been found so we have a look if
    # any parent molecules have any drug targets
    parents = get_parent_molecules(session, chembl_id)

    for p in parents:
        drug_targets.extend(
            [(i, chembl_id, 'parent') for i in get_drug_targets(
                session, p.chembl_id)])

    if len(drug_targets) > 0:
        return drug_targets

    # If we get here then no drug targets have been found with the parent
    # compounds so we have a look if any active molecules have any drug targets
    active = get_parent_molecules(session, chembl_id)

    for a in active:
        drug_targets.extend(
            [(i, chembl_id, 'active') for i in get_drug_targets(
                session, a.chembl_id)])

    return drug_targets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_parent_molecules(session, chembl_id):
    """
    Find parent molecules for a chembl ID. This queries the molecule_hierarchy
    table for active molecules matching the chembl_id and returns the parent

    Parameters
    ----------

    session : :obj:`Session`
        The SQL alchemy session
    chembl_id :`str`
        The chembl ID to get the efficacy targets for
    """

    sql = text("""select md2.*
    from molecule_dictionary md
    join molecule_hierarchy mh
    on mh.active_molregno = md.molregno
    join molecule_dictionary md2
    on md2.molregno = mh.parent_molregno
    where md.chembl_id = :DRUG
    and mh.parent_molregno != mh.active_molregno""")

    query = session.execute(sql, {'DRUG': chembl_id})

    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_active_molecules(session, chembl_id):
    """
    Find active molecules for a chembl ID. This queries the molecule_hierarchy
    table for parent molecules matching the chembl_id and returns the active
    molecule

    Parameters
    ----------

    session : :obj:`Session`
        The SQL alchemy session
    chembl_id :`str`
        The chembl ID to get the efficacy targets for
    """

    sql = text("""select md2.*
    from molecule_dictionary md
    join molecule_hierarchy mh
    on mh.parent_molregno = md.molregno
    join molecule_dictionary md2
    on md2.molregno = mh.active_molregno
    where md.chembl_id = :DRUG
    and mh.parent_molregno != mh.active_molregno""")

    query = session.execute(sql, {'DRUG': chembl_id})

    return query.fetchall()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def map_drug_name(session, drug_name, exhaustive=True):
    """
    """
    sql = text("""select md.pref_name as compound_pref_name
    , synonyms
    , syn_type
    , md.chembl_id as "compound_chembl_id"
    , molecule_type
    , indication_class
    from molecule_synonyms ms
    join molecule_dictionary md
    on ms.molregno = md.molregno
    where ms.synonyms = :DRUG
    or md.pref_name = :DRUG
    """)
    query = session.execute(sql, {'DRUG': drug_name})

    # Do the query
    mappings = query.fetchall()

    if len(mappings) > 0:
        return mappings

    if exhaustive is False:
        return []

    # If we get here then we want to attempt to process in someway to see if we
    # can get a mapping

    # First we substitute all the various things that we can to get a mapping
    # brackets first
    drug_name, subs = re.subn(r'\s*\(.+?\)\s*$', '', drug_name)
    if subs > 0:
        return map_drug_name(session, drug_name, exhaustive=True)

    if re.search(r'\s', drug_name):
        for i in END_SUBS:
            # Here we only test the first substitution
            drug_name, subs = i.subn('', drug_name)
            if subs > 0:
                return map_drug_name(session, drug_name, exhaustive=False)
    else:
        for i in START_SUBS:
            drug_name, subs = i.subn('', drug_name)
            if subs > 0:
                return map_drug_name(session, drug_name, exhaustive=False)

    return []
