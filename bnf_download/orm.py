"""SQLAlchemy ORM classes for representing the BNF data
"""
# from umls_tools import orm_mixin
from sqlalchemy_config import utils
from umls_tools import orm_mixin
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    ForeignKey,
    Column,
    Integer,
    SmallInteger,
    String,
    Text,
    Boolean,
    Index,
    DateTime,
    Sequence
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Version(Base):
    """A representation of the BNF ``version`` table.

    Parameters
    ----------
    version_id : `int`
        Auto-incremented primary key.
    version_number : `str`, optional, default: `NoneType`
        The version number, this will apply to other resources such as
        ChEMBL not the BNF (length 255).
    version_date : `datetime.datetime`
        The version date.
    """
    __tablename__ = 'version'

    version_id = Column(
        Integer, Sequence("version_id_seq"), nullable=False, primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    version_number = Column(
        String(255), nullable=True,
        doc="The version number, this will apply to other resources such as "
        "ChEMBL not the BNF."
    )
    version_date = Column(
        DateTime, nullable=False, unique=True,
        doc="The date the BNF website was last updated."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Drug(Base):
    """A representation of the BNF ``drug`` table.

    Parameters
    ----------
    drug_id : `int`
        Auto-incremented primary key.
    drug_name : `str`
        The drug name, note that some drugs are combination drugs, so this is
        not the same as compound name. This is indexed (length 255).
    drug_url : `str`
        The source URL at the BNF website for the drug (length text).
    drug_component : `bnf_download.bnf_orm.DrugComponent`, optional, \
    default: `NoneType`
        Relationship with ``drug_component``.
    drug_effect : `bnf_download.bnf_orm.DrugEffect`, optional, \
    default: `NoneType`
        Relationship with ``drug_effect``.

    Notes
    -----
    This is the main drug table in the database. The drug names are as they
    appear on the website. For example, combination drugs are available in this
    table.
    """
    __tablename__ = 'drug'

    drug_id = Column(
        Integer, Sequence("drug_id_seq"), nullable=False, primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_name = Column(
        String(255), nullable=False, index=True, unique=True,
        doc="The drug name, note that some drugs are combination drugs, so "
        "this is not the same as compound name."
    )
    drug_url = Column(
        Text, nullable=False,
        doc="The source URL at the BNF website for the drug."
    )
    drug_component = relationship(
        "DrugComponent",
        back_populates='drug',
        doc="The relationship back to the ``drug_component`` table."
    )
    drug_effect = relationship(
        "DrugEffect",
        back_populates='drug',
        doc="The relationship back to the ``drug_effect`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugComponent(Base):
    """A representation of the BNF ``drug_component`` table.

    Parameters
    ----------
    drug_component_id : `int`
        Auto-incremented primary key.
    drug_id : `int`
        Foreign key link to the ``drug`` table. Foreign key to
        ``drug.drug_id``.
    drug_component_name : `str`
        The name of the drug component, if a drug has a single component then
        this will be the same as the drug name, otherwise it will be a sub-
        string of the drug name. This is indexed (length 255).
    drug : `bnf_download.bnf_orm.Drug`, optional, default: `NoneType`
        Relationship with ``drug``.
    chembl_mapping : `bnf_download.bnf_orm.ChemblMapping`, optional, \
    default: `NoneType`
        Relationship with ``chembl_mapping``.

    Notes
    -----
    The drug_component table is a breakdown of all the drugs in the ``drug``
    table. For most drugs the drug component name and the drug name will be the
    same. However, for combination drugs, the individual components of the
    combination will be represented in this table.
    """
    __tablename__ = 'drug_component'

    drug_component_id = Column(
        Integer, Sequence("drug_component_id_seq"), nullable=False,
        primary_key=True, autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_id = Column(
        Integer, ForeignKey('drug.drug_id'),
        nullable=False,
        doc="Foreign key link to the ``drug`` table."
    )
    drug_component_name = Column(
        String(255), nullable=False, index=True,
        doc="The name of the drug component, if a drug has a single component"
        " then this will be the same as the drug name, otherwise it will be a"
        " sub-string of the drug name."
    )

    drug = relationship(
        "Drug",
        back_populates='drug_component',
        doc="The relationship back to the ``drug`` table."
    )
    chembl_mapping = relationship(
        "ChemblMapping",
        back_populates='drug_component',
        doc="The relationship back to the ``chembl_mapping`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugEffect(Base):
    """A representation of the BNF drug effect table, this table logs
    indications and adverse effects.

    Parameters
    ----------
    drug_effect_id : `int`
        Auto-incremented primary key.
    drug_id : `int`
        Foreign key link to the ``drug`` table. Foreign key to
        ``drug.drug_id``.
    drug_effect_type : `str`
        The effect type, either indication or side_effect (length 20).
    drug_effect_freq : `str`
        The qualitative frequency of the drug effect. Indications will have the
        value all, side effects will have the value represented in the BNF
        (length 30).
    drug_effect_name : `str`
        The name of the indication/side effect, i.e. the disease it treats or
        potentially causes (length text).
    drug_class_effect : `bool`
        Is the drug effect an effect of all drugs in the class or just this
        drug.
    drug_class : `str`, optional, default: `NoneType`
        The class of drug (length 255).
    drug : `bnf_download.orm.Drug`, optional, default: `NoneType`
        Relationship with ``drug``.
    drug_roa `bnf_download.orm.DrugRoa``
        Relationship back to the ``drug_roa`` table.
    drug_effect_index : `bnf_download.orm.TermIndexMap`
        Relationship back to the ``term_index_mapping`` table.

    Notes
    -----
    A drug effect is any effect that is associated with a drug. This could be
    indications, side effects, contra-indications, cautions. The
    ``drug_effect_freq`` - is the qualitative frequency for side effects. This
    will have the value ``all`` for indications and ``info`` for some expanded
    side effect information.
    """
    __tablename__ = 'drug_effect'
    # For MySQL indexing of text fields
    __table_args__ = (
        Index("idx_den", "drug_effect_name", mysql_length=1024),
    )

    drug_effect_id = Column(
        Integer, Sequence('drug_effect_id_seq'), nullable=False,
        primary_key=True, autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_id = Column(
        Integer, ForeignKey('drug.drug_id'),
        nullable=False,
        doc="Foreign key link to the ``drug`` table."
    )
    drug_effect_type = Column(
        String(20), nullable=False,
        doc="The effect type, either indication or side_effect."
    )
    drug_effect_freq = Column(
        String(30), nullable=False, default="all",
        doc="The qualitative frequency of the drug effect. Indications will"
        " have the value all, side effects will have the value represented"
        " in the BNF."
    )
    drug_effect_name = Column(
        Text, nullable=False,
        doc="The name of the indication/side effect, i.e. the disease it"
        " treats or potentially causes."
    )
    drug_class_effect = Column(
        Boolean, nullable=False, default=False,
        doc="Is the drug effect an effect of all drugs in the class or just"
        " this drug."
    )
    drug_class = Column(
        String(255), nullable=True,
        doc="The class of drug."
    )

    drug = relationship(
        "Drug",
        back_populates='drug_effect',
        doc="Relationship back to the ``drug`` table."
    )
    drug_roa = relationship(
        "DrugRoa",
        back_populates="drug_effect",
        doc="Relationship back to the ``drug_roa`` table."
    )
    drug_effect_index = relationship(
        "TermIndexMap",
        back_populates="drug_effect",
        primaryjoin='DrugEffect.drug_effect_id == TermIndexMap.term_id',
        foreign_keys='DrugEffect.drug_effect_id',
        doc="Relationship back to the ``term_index_mapping`` table."
    )
    # drug_effect_mapping = relationship(
    #     "DrugEffectMapping",
    #     back_populates='drug_effect',
    #     doc="Relationship back to the ``drug_effect_mapping`` table."
    # )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugRoa(Base):
    """A representation of the BNF drug ROA and dosage table.

    Parameters
    ----------
    drug_roa_id : `int`
        Auto-incremented primary key.
    drug_effect_id : `int`
        Foreign key link to the ``drug`` table. Foreign key to
        ``drug.drug_id``.
    roa : `str`
        Route of administration (length 255).
    patient_group : `str`
        The patient group that the dosage applies to (length 255).
    dosage_desc : `str`
        The dosage description string (length Text).
    drug_effect : `bnf_download.orm.DrugEffect`
        Relationship back to the ``drug_effect`` table.

    Notes
    -----
    This contains the route of administration and dosage data as free text.
    Note, that the route of ROA/dosage data is linked to a drug effect and not
    a drug. This reflects the arrangement on the website. Presumably the dosage
    might be different for different indications?
    """
    __tablename__ = 'drug_roa'
    # For MySQL indexing of text fields
    __table_args__ = (
        Index("idx_dosage", "dosage_desc", mysql_length=1024),
    )

    drug_roa_id = Column(
        Integer, Sequence('drug_roa_id'), nullable=False, primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_effect_id = Column(
        Integer, ForeignKey('drug_effect.drug_effect_id'),
        nullable=False,
        doc="Foreign key link to the drug effect table."
    )
    roa = Column(
        String(255), doc="The route of administration."
    )
    patient_group = Column(
        String(255), doc="The patient group that the dosage applies to."
    )
    dosage_desc = Column(
        Text, doc="The dosage description."
    )

    drug_effect = relationship(
        "DrugEffect",
        back_populates="drug_roa",
        doc="Relationship back to the ``drug_effect`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class DrugEffectMapping(Base):
#     """A representation of the BNF ``drug_effect_mapping`` table. This maps
#     drug effects with UMLS concept identifiers.

#     Parameters
#     ----------
#     drug_effect_mapping_id : `int`
#         Auto-incremented primary key.
#     drug_effect_id : `int`
#         Foreign key link to the ``drug_effect`` table. Foreign key to
#         ``drug_effect.drug_effect_id``.
#     drug_effect_mapping_cui : `str`
#         The UMLS concept identifier (CUI) for the drug effect (length 8).
#     drug_effect_mapping_term : `str`
#         The UMLS term for the CUI (length 8).
#     drug_effect : `bnf_download.bnf_orm.DrugEffect`, optional, \
#     default: `NoneType`
#         Relationship with ``drug_effect``.
#     """
#     __tablename__ = 'drug_effect_mapping'

#     drug_effect_mapping_id = Column(
#         Integer, Sequence('drug_effect_mapping_id_seq'), nullable=False,
#         primary_key=True,
#         autoincrement=True,
#         doc="Auto-incremented primary key."
#     )
#     drug_effect_id = Column(
#         Integer,
#         ForeignKey('drug_effect.drug_effect_id'),
#         nullable=False,
#         doc="Foreign key link to the ``drug_effect`` table."
#     )
#     drug_effect_mapping_cui = Column(
#         String(8), nullable=False,
#         doc="The UMLS concept identifier (CUI) for the drug effect."
#     )
#     drug_effect_mapping_term = Column(
#         String(8), nullable=False,
#         doc="The UMLS term for the CUI."
#     )

#     drug_effect = relationship(
#         "DrugEffect",
#         back_populates='drug_effect_mapping',
#         doc="Relationship back to the ``drug_effect`` table."
#     )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugEffectAnnotation(Base):
    """A representation of the BNF ``drug_effect_annotation`` table. This maps
    annotation strings to annotation labels.

    Parameters
    ----------
    drug_effect_annotation_id : `int`
        Auto-incremented primary key.
    is_checked : `bool`
        Has the drug effect been checked.
    is_invalid : `bool`
        Has the drug effect been evaluated to be invalid.
    drug_effect_name : `str`
        The drug effect name (length 2000).
    drug_effect_type : `str`
        The effect type, either indication or side_effect (length 20).
    notes : `str`, optional, default: `NoneType`
        Any user notes for the annotation (length text).
    annotation_manual_label : `bnf_download.orm.AnnotationManualLabel`, \
    optional, default: `NoneType`
        Relationship back to the ``annotation_manual_label`` table.
    annotation_auto_label : `bnf_download.orm.AnnotationAutoLabel`, optional,
    default: `NoneType`
        Relationship back to the ``annotation_auto_label`` table.
    """
    __tablename__ = 'drug_effect_annotation'

    drug_effect_annotation_id = Column(
        Integer, Sequence('drug_effect_annotation_id_seq'),
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    is_checked = Column(
        Boolean,
        nullable=False,
        default=False,
        doc="Has the drug effect been checked."
    )
    is_invalid = Column(
        Boolean,
        nullable=False,
        default=False,
        doc="Has the drug effect been evaluated to be invalid."
    )
    drug_effect_name = Column(
        String(2000), nullable=False,
        doc="The drug effect name."
    )
    drug_effect_type = Column(
        String(20), nullable=False,
        doc="The effect type, either indication or side_effect."
    )
    notes = Column(
        Text, nullable=True,
        doc="Any user notes for the annotation"
    )
    annotation_manual_label = relationship(
        "AnnotationManualLabel",
        back_populates='drug_effect_annotation',
        doc="Relationship back to the ``annotation_manual_label`` table."
    )
    annotation_auto_label = relationship(
        "AnnotationAutoLabel",
        back_populates='drug_effect_annotation',
        doc="Relationship back to the ``annotation_auto_label`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AnnotationAutoLabel(Base):
    """A representation annotation label coordinates that have been assigned
    computationally.

    Parameters
    ----------
    auto_label_id : `int`
        Auto-incremented primary key.
    drug_effect_annotation_id : `int`
        Foreign key to the ``drug_effect_annotation`` table. Foreign key to
        ``drug_effect_annotation.drug_effect_annotation_id``.
    start_pos : `int`
        The start position of the annotation label.
    end_pos : `int`
        The end position of the annotation label.
    drug_effect_annotation : `bnf_download.orm.DrugEffectAnnotation`, \
    optional, default: `NoneType`
        Relationship back to the ``drug_effect_annotation`` table.
    auto_label_class : `bnf_download.orm.AutoLabelClass`, optional,\
    default: `NoneType`
        Relationship back to the ``auto_label_class`` table.
    """
    __tablename__ = 'annotation_auto_label'

    auto_label_id = Column(
        Integer, Sequence('auto_label_id_seq'),
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_effect_annotation_id = Column(
        Integer,
        ForeignKey('drug_effect_annotation.drug_effect_annotation_id'),
        nullable=False,
        doc="Foreign key to the ``drug_effect_annotation`` table."
    )
    start_pos = Column(
        Integer,
        nullable=False,
        default=-1,
        doc="The start position of the annotation label."
    )
    end_pos = Column(
        Integer,
        nullable=False,
        default=-1,
        doc="The end position of the annotation label."
    )
    drug_effect_annotation = relationship(
        "DrugEffectAnnotation",
        back_populates='annotation_auto_label',
        doc="Relationship back to the ``drug_effect_annotation`` table."
    )
    auto_label_class = relationship(
        "AutoLabelClass",
        back_populates='annotation_auto_label',
        doc="Relationship back to the ``auto_label_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AutoLabelClass(Base):
    """A representation of the ``auto_label_class`` table. This maps auto
    assigned labels to class types.

    Parameters
    ----------
    auto_label_class_id : `int`
        Auto-incremented primary key.
    auto_label_id : `int`
        Foreign key back to ``annotation_auto_label``. Foreign key to
        ``annotation_auto_label.auto_label_id``.
    label_class_id : `int`
        Foreign key back to ``label_class``. Foreign key to
        ``label_class.label_class_id``.
    annotation_auto_label : `bnf_download.orm.AnnotationAutoLabel`, optional,\
    default: `NoneType`
        Relationship back to the ``annotation_auto_label`` table.
    label_class : `bnf_download.orm.LabelClass`, optional, default: `NoneType`
        Relationship back to the ``label_class`` table.
    """
    __tablename__ = 'auto_label_class'

    auto_label_class_id = Column(
        Integer, Sequence('auto_label_id_seq'), nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    auto_label_id = Column(
        Integer, ForeignKey('annotation_auto_label.auto_label_id'),
        nullable=False,
        doc="Foreign key back to ``annotation_auto_label``."
    )
    label_class_id = Column(
        Integer,
        ForeignKey('label_class.label_class_id'),
        nullable=False,
        doc="Foreign key back to ``label_class``."
    )
    annotation_auto_label = relationship(
        "AnnotationAutoLabel",
        back_populates='auto_label_class',
        doc="Relationship back to the ``annotation_auto_label`` table."
    )
    label_class = relationship(
        "LabelClass",
        back_populates='auto_label_class',
        doc="Relationship back to the ``label_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AnnotationManualLabel(Base):
    """A representation annotation label coordinates that have been assigned or
    checked manually.

    Parameters
    ----------
    manual_label_id : `int`
        Auto-incremented primary key.
    drug_effect_annotation_id : `int`
        Foreign key to the ``drug_effect_annotation`` table. Foreign key to
        ``drug_effect_annotation.drug_effect_annotation_id``.
    start_pos : `int`
        The start position of the annotation label.
    end_pos : `int`
        The end position of the annotation label.
    drug_effect_annotation : `bnf_download.orm.DrugEffectAnnotation`, \
    optional, default: `NoneType`
        Relationship back to the ``drug_effect_annotation`` table.
    manual_label_class : `bnf_download.orm.ManualLabelClass`, optional,\
    default: `NoneType`
        Relationship back to the ``manual_label_class`` table.
    """
    __tablename__ = 'annotation_manual_label'

    manual_label_id = Column(
        Integer, Sequence('manual_label_id_seq'),
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_effect_annotation_id = Column(
        Integer,
        ForeignKey('drug_effect_annotation.drug_effect_annotation_id'),
        nullable=False,
        doc="Foreign key to the ``drug_effect_annotation`` table."
    )
    start_pos = Column(
        Integer,
        nullable=False,
        default=-1,
        doc="The start position of the annotation label."
    )
    end_pos = Column(
        Integer,
        nullable=False,
        default=-1,
        doc="The end position of the annotation label."
    )
    drug_effect_annotation = relationship(
        "DrugEffectAnnotation",
        back_populates='annotation_manual_label',
        doc="Relationship back to the ``drug_effect_annotation`` table."
    )
    manual_label_class = relationship(
        "ManualLabelClass",
        back_populates='annotation_manual_label',
        doc="Relationship back to the ``manual_label_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualLabelClass(Base):
    """A representation of the ``manual_label_class`` table. This maps auto
    assigned labels to class types.

    Parameters
    ----------
    manual_label_class_id : `int`
        Auto-incremented primary key.
    manual_label_id : `int`
        Foreign key back to ``annotation_manual_label``. Foreign key to
        ``annotation_manual_label.manual_label_id``.
    label_class_id : `int`
        Foreign key back to ``label_class``. Foreign key to
        ``label_class.label_class_id``.
    annotation_manual_label : `bnf_download.orm.AnnotationManualLabel`,\
    optional, default: `NoneType`
        Relationship back to the ``annotation_manual_label`` table.
    label_class : `bnf_download.orm.LabelClass`, optional, default: `NoneType`
        Relationship back to the ``label_class`` table.
    """
    __tablename__ = 'manual_label_class'

    manual_label_class_id = Column(
        Integer, Sequence('auto_label_id_seq'), nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    manual_label_id = Column(
        Integer, ForeignKey('annotation_manual_label.manual_label_id'),
        nullable=False,
        doc="Foreign key back to ``annotation_manual_label``."
    )
    label_class_id = Column(
        Integer,
        ForeignKey('label_class.label_class_id'),
        nullable=False,
        doc="Foreign key back to ``label_class``."
    )
    annotation_manual_label = relationship(
        "AnnotationManualLabel",
        back_populates='manual_label_class',
        doc="Relationship back to the ``annotation_manual_label`` table."
    )
    label_class = relationship(
        "LabelClass",
        back_populates='manual_label_class',
        doc="Relationship back to the ``label_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class LabelClass(Base):
    """A representation of the ``label_class`` lookup table. This maps label
    class IDs to label class names.

    Parameters
    ----------
    label_class_id : `int`
        Auto-incremented primary key.
    label_class_name : `str`
        The string name for the label (length 255).
    manual_label_class : `bnf_download.orm.ManualLabelClass`, optional, \
    default: `NoneType`
        Relationship back to the ``manual_label_class`` table.
    auto_label_class : `bnf_download.orm.AutoLabelClass`, optional, \
    default: `NoneType`
        Relationship back to the ``auto_label_class`` table.
    """
    __tablename__ = 'label_class'

    label_class_id = Column(
        Integer, Sequence('label_class_id_seq'),
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    label_class_name = Column(
        String(255),
        nullable=False,
        doc="The string name for the label."
    )
    manual_label_class = relationship(
        "ManualLabelClass",
        back_populates='label_class',
        doc="Relationship back to the ``manual_label_class`` table."
    )
    auto_label_class = relationship(
        "AutoLabelClass",
        back_populates='label_class',
        doc="Relationship back to the ``auto_label_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChemblMapping(Base):
    """A representation of BNF ``chembl_mapping`` table. This maps BNF drug
    components to CHEMBL compound IDs.

    Parameters
    ----------
    chembl_mapping_id : `int`
        Auto-incremented primary key.
    drug_component_id : `int`
        Foreign key link to the ``drug_component`` table. Foreign key to
        ``drug_component.drug_component_id``.
    full_mapping : `bool`
        Is the ChEMBL map based on the full drug component name or just the
        active moiety minus the salt.
    mapping_synonym : `str`
        The ChEMBL synonym name that mapped to the drug component (length 200).
    compound_pref_name : `str`, optional, default: `NoneType`
        The ChEMBL preferred name for the drug component (length 255).
    compound_chembl_id : `str`
        The ChEMBL ID for the compound that maps to the drug component (length
        20).
    molecule_type : `str`
        The molecule type for the drug component (length 30).
    indication_class : `str`, optional, default: `NoneType`
        The ChEMBL indication class for the drug component (length text).
    drug_component : `bnf_download.bnf_orm.DrugComponent`, optional, \
    default: `NoneType`
        Relationship with ``drug_component``.
    chembl_target_components : `bnf_download.bnf_orm.ChemblTargetComponents`, \
    optional, default: `NoneType`
        Relationship with ``chembl_target_components``.

    Notes
    -----
    The ChEMBL mappings are mapped back to the BNF drug components as drug
    combinations are unlikely to map directly into ChEMBL.
    """
    __tablename__ = 'chembl_mapping'

    chembl_mapping_id = Column(
        Integer, Sequence('chembl_mapping_id_seq'), nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    drug_component_id = Column(
        Integer,
        ForeignKey('drug_component.drug_component_id'),
        nullable=False,
        doc="Foreign key link to the ``drug_component`` table."
    )
    full_mapping = Column(
        Boolean, nullable=False,
        doc="Is the ChEMBL map based on the full drug component name or"
        " just the active moiety minus the salt."
    )
    mapping_synonym = Column(
        String(200), nullable=False,
        doc="The ChEMBL synonym name that mapped to the drug component"
    )
    compound_pref_name = Column(
        String(255), nullable=True,
        doc="The ChEMBL preferred name for the drug component."
    )
    compound_chembl_id = Column(
        String(20), nullable=False,
        doc="The ChEMBL ID for the compound that maps to the drug component."
    )
    molecule_type = Column(
        String(30), nullable=False,
        doc="The molecule type for the drug component."
    )
    indication_class = Column(
        Text, nullable=True,
        doc="The ChEMBL indication class for the drug component."
    )

    drug_component = relationship(
        "DrugComponent",
        back_populates='chembl_mapping',
        doc="Relationship back to the ``drug_components`` table."
    )
    chembl_target_components = relationship(
        "ChemblTargetComponents",
        back_populates='chembl_mapping',
        doc="Relationship back to the ``chembl_target_components`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChemblTargetComponents(Base):
    """A representation of the BNF ``chembl_target_components`` table. This
    ChEMBL targets to target components.

    Parameters
    ----------
    chembl_target_component_id : `int`
        Auto-incremented primary key.
    chembl_mapping_id : `int`
        Foreign key link to the ``chembl_mapping`` table. Foreign key to
        ``chembl_mapping.chembl_mapping_id``.
    target_mapping_compound_chembl_id : `str`
        The ChEMBL ID for the for the compound that maps to a BNF drug
        component (length 20).
    target_mapping_relationship : `str`
        TBC.. (length 10).
    mechanism_of_action : `str`, optional, default: `NoneType`
        The mechanism is action for the for the compound that maps to a BNF
        drug component (length 255).
    target_chembl_id : `str`
        The ChEMBL ID for the efficacy target for the compound that maps to a
        BNF drug component (length 20).
    target_type : `str`
        The target type for the efficacy target for the compound that maps to a
        BNF drug component (length 30).
    target_pref_name : `str`
        The target preferred name for the efficacy target for the compound that
        maps to a BNF drug component (length 200).
    organism : `str`
        The organism for the efficacy target for the compound that maps to a
        BNF drug component. Not all will be human targets, i.e. antibiotics and
        microbes (length 150).
    action_type : `str`, optional, default: `NoneType`
        The action type for the compound on the efficacy target, i.e.
        agonist/antagonist (length 50).
    target_component_database : `str`, optional, default: `NoneType`
        The database that references the target component. This is typically a
        uniprot identifier for the efficacy target component of the
        compound that maps to a BNF drug component (length 25).
    target_accession : `str`, optional, default: `NoneType`
        The accession ID for the target compoenent, this is typically a uniprot
        identifier for the efficacy target component of the compound that maps
        to a BNF drug component (length 25).
    target_description : `str`, optional, default: `NoneType`
        The description for the target component, this is typically a uniprot
        description for the uniprot identifier of the efficacy target component
        (length 200).
    chembl_mapping : `bnf_download.bnf_orm.ChemblMapping`, optional, \
    default: `NoneType`
        Relationship with ``chembl_mapping``.

    Notes
    -----
    This links the ChEMBL compounds in the chembl_mapping table to ChEMBL
    targets and the target components. ChEMBL targets are not necessarily
    single proteins (or even proteins at all). Therefore a ChEMBL target can
    have many individual protein components (i.e. could be a protein complex).
    """
    __tablename__ = 'chembl_target_components'

    chembl_target_component_id = Column(
        Integer, Sequence('chembl_target_component_id_seq'), nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    chembl_mapping_id = Column(
        Integer,
        ForeignKey('chembl_mapping.chembl_mapping_id'),
        nullable=False,
        doc="Foreign key link to the ``chembl_mapping`` table."
    )
    target_mapping_compound_chembl_id = Column(
        String(20),
        nullable=False,
        doc="The ChEMBL ID for the for the compound that maps to a BNF drug"
        " component."
    )
    target_mapping_relationship = Column(
        String(10), nullable=False,
        doc="TBC..."
    )
    mechanism_of_action = Column(
        String(255),
        doc="The mechanism is action for the for the compound that maps to"
        " a BNF drug component."
    )
    target_chembl_id = Column(
        String(20), nullable=False,
        doc="The ChEMBL ID for the efficacy target for the compound that maps"
        " to a BNF drug component."
    )
    target_type = Column(
        String(30), nullable=False,
        doc="The target type for the efficacy target for the compound that"
        " maps to a BNF drug component."

    )
    target_pref_name = Column(
        String(200), nullable=False,
        doc="The target preferred name for the efficacy target for the"
        " compound that maps to a BNF drug component."
    )
    organism = Column(
        String(150), nullable=False,
        doc="The organism for the efficacy target for the compound that maps"
        " to a BNF drug component. Not all will be human targets, i.e."
        " antibiotics and microbes."

    )
    action_type = Column(
        String(50), nullable=True,
        doc="The action type for the compound on the efficacy target,"
        " i.e. agonist/antagonist."
    )
    target_component_database = Column(
        String(255), nullable=True,
        doc="The database that references the target component. This is "
        "typically a uniprot identifier for the efficacy target component"
        " of the compound that maps to a BNF drug component."
    )
    target_accession = Column(
        String(25), nullable=True,
        doc="The accession ID for the target compoenent, this is typically"
        " a uniprot identifier for the efficacy target component of the "
        "compound that maps to a BNF drug component."

    )
    target_description = Column(
        String(200), nullable=True,
        doc="The description for the target component, this is typically a "
        "uniprot description for the uniprot identifier of the efficacy target"
        " component."
    )

    chembl_mapping = relationship(
        "ChemblMapping",
        back_populates='chembl_target_components',
        doc="Relationship back to the ``chembl_mapping`` table."
    )
    # ensembl_genes = relationship(
    #     "EnsemblGenes",
    #     back_populates='chembl_target_components',
    #     doc="Relationship back to the ``ensembl_genes`` table."
    # )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EnsemblGenes(Base):
    """A representation of the BNF ``ensembl_genes`` table. This maps protein
    ChEMBL target components to ensembl gene identifiers for the human targets.

    Parameters
    ----------
    gene_id : `int`
        Auto-incremented primary key.
    target_accession : `str`
        The uniprot identifier for the efficacy target that was used to map to
        the Ensembl gene (length 25).
    ensembl_gene_id : `str`
        The Ensembl gene ID that maps to the uniprot ID (length 128).
    chr_name : `str`
        The chromosome name containing the gene (length 40).
    start_pos : `int`
        The start position of the gene in bp.
    end_pos : `int`
        The end position of the genein bp.
    strand : `int`
        The strand of the gene.
    assembly_name : `str`
        The genome assembly of the gene coordinates (length 10).
    ensembl_description : `str`, optional, default: `NoneType`
        The long form description of the gene (length text).

    Notes
    -----
    Although the content for this table is based on the targets in the
    ``chembl_target_components`` there is no direct link between the two.
    """
    __tablename__ = 'ensembl_genes'

    gene_id = Column(
        Integer, Sequence('gene_id_seq'), nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="Auto-incremented primary key."
    )
    target_accession = Column(
        String(25), nullable=False,
        doc="The target identifier for the efficacy target that was used"
        " to map to the Ensembl gene."
    )
    # chembl_target_component_id = Column(
    #     Integer,
    #     ForeignKey('chembl_target_components.chembl_target_component_id'),
    #     nullable=False
    # )
    ensembl_gene_id = Column(
        String(128), nullable=False,
        doc="The Ensembl gene ID that maps to the uniprot ID."
    )
    chr_name = Column(
        String(40), nullable=False,
        doc="The chromosome name containing the gene."
    )
    start_pos = Column(
        Integer, nullable=False,
        doc="The start position of the gene in bp."
    )
    end_pos = Column(
        Integer, nullable=False,
        doc="The end position of the genein bp."
    )
    strand = Column(
        SmallInteger, nullable=False,
        doc="The strand of the gene."
    )
    assembly_name = Column(
        String(10), nullable=False,
        doc="The genome assembly of the gene coordinates."
    )
    ensembl_description = Column(
        Text,
        doc="The long form description of the gene."
    )

    # chembl_target_components = relationship("ChemblTargetComponents",
    #                                         back_populates=__tablename__)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TermIndexLookup(Base, orm_mixin.TermIndexLookupMixin):
    """A representation of the ``term_index_lookup`` table.

    Parameters
    ----------
    token_id : `int`
        Auto-incremented primary key.
    token_str : `str`
        The actual lookup string of the token.
    token_count `int`
        The total count of the token in the dataset.
    token_prob : `int`
        The prbability that the token is encountered in the dataset.
    token_index_map : `umls_tools.orm.TermIndexMap`
        The relationship back to the ``token_index_map`` table.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TermIndexMap(Base, orm_mixin.TermIndexMapMixinInt):
    """A representation of the ``term_index_mapping`` table.

    Parameters
    ----------
    mapping_id : `int`
        Auto-incremented primary key.
    token_id : `int`
        A mapping back to a specific token.
    start_pos : `int`
        The start position of the word represented by the token in the term
        string.
    end_pos : `int`
        The end position of the word represented by the token in the term
        string.
    rank_order : `int`
        The rank order position of the word represented by the token in the
        term string.
    term_id : `int`
        The ID for the term identifier. This maps back to the
        ``drug_effect.drug_effect_id``.
    token_index_lookup : `bnf_download.orm.TermIndexLookup`
        The relationship back to the ``token_index_lookup`` table.
    drug_effect : `bnf_download.orm.DrugEffect`
        Relationship back to the ``drug_effect`` table.
    """
    drug_effect = relationship(
        "DrugEffect",
        back_populates="drug_effect_index",
        primaryjoin='DrugEffect.drug_effect_id == TermIndexMap.term_id',
        foreign_keys='DrugEffect.drug_effect_id',
        doc="Relationship back to the ``drug_effect`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return utils.print_orm_obj(self)
