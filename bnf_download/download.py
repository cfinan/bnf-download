"""Download data from the public NICE BNF website into a relational database.
There is no API so it needs to be scraped. There are options to map the drugs
back to ChEMBL and their targets and to map targets back to Ensembl genes.
"""
from bnf_download import (
    __version__,
    __name__ as pkg_name,
    queries as bq,
    orm as bo,
    bnf,
)
from umls_tools.admin import index
from chembl_orm import queries as cq
from sqlalchemy_config import config as cfg
from sqlalchemy.orm.exc import NoResultFound
from pyaddons import log, utils
from datetime import datetime
from ensembl_rest_client import client
from tqdm import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm
import time
import random
import argparse
import os
import sys
import pprint as pp

# Prefixes in config files
BNF_PREFIX = "db."
"""The SQLAlchemy database BNF command prefixes in the config file (`str`)
"""
CHEMBL_PREFIX = "db."
"""The SQLAlchemy database ChEMBL command prefixes in the config file (`str`)
"""
_PROG_NAME = 'bnf-download'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """Main entry point for the downloader. For API access see
    `bnf_download.download.download_bnf`
    """
    # Sort out all the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    bnf_sm = cfg.get_new_sessionmaker(
        args.db, config_default=args.config, exists=False
    )

    # Get the session to query/load
    bnf_session = bnf_sm()

    # Make sure all the tables are created
    bo.Drug().metadata.create_all(bnf_session.get_bind())

    chembl_query = None
    if args.chembl is not None:
        # Get a sessionmaker to create sessions to interact with the database
        chembl_sm = cfg.get_new_sessionmaker(
            args.chembl, conn_prefix=CHEMBL_PREFIX,
            config_arg=args.config, case_sensitive=False,
            exists=True
        )

        # Get the session to query/load
        chembl_query = cq.ChemblQuery(chembl_sm)
        chembl_query.open()

    try:
        download_bnf(
            bnf_session, chembl_query=chembl_query, debug=args.debug,
            verbose=args.verbose, randdrug=args.randdrug, use_drugs=args.drugs,
            randwait=args.randwait, ensembl_rest_url=args.ensembl_rest,
            tmpdir=args.tmp
        )
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        bnf_session.close()
        if chembl_query is not None:
            chembl_query.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments.

    Returns
    -------
    parser : `argpare.ArgumentParser`
        The "unparsed" argument parser
    """

    parser = argparse.ArgumentParser(
        description="A scraper/parser for the BNF"
    )
    parser.add_argument(
        'db', type=str, nargs="?",
        help="A section header in the config file '~/.bnf.cnf' or a path to"
        " an SQLite database. If it is a section header then this should "
        "describe the SQLAlchemy connection options, prefixed with '{0}', "
        "i.e. '{0}url' If the database exists only new drugs are prcocessed."
        " No attempt is made to update information for existing drugs. If no"
        " file is given then an SQLite database is created in the root of"
        " your home directory with the name '~/bnf_YYYYMMDD.db'".format(
            BNF_PREFIX
        )
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default=cfg.DEFAULT_CONFIG
    )
    parser.add_argument(
        '-d', '--debug',
        type=int,
        default=-1,
        help="only process the first --debug entries"
    )
    parser.add_argument(
        '-D', '--drugs', type=str, nargs='+',
        help="Only process specific drug entries. If --debug is used this is"
        " applied to the list of drugs supplied here. The drug names should"
        " be supplied as specified on the BNF website, although they are"
        " case in-sensitive."
    )
    parser.add_argument(
        '-v', '--verbose',
        action="count",
        help="Log progress, use -vv to turn on progress monitors."
    )
    parser.add_argument(
        '-r',
        '--randdrug',
        action="store_true",
        help="process drugs in a random order - to avoid hitting the same"
        " webpage while debugging"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will "
        "use the system tmp"
    )
    parser.add_argument(
        '-w', '--randwait',
        type=float,
        default=2.0,
        help="the maximum random wait time betweeen drug scrapes"
    )
    parser.add_argument(
        '-m', '--chembl',
        type=str,
        default=None,
        help="SQLalchemy path or config header for CHEMBL, config options in"
        " CHEMBL should be prefixed with '{0}'".format(CHEMBL_PREFIX)
    )
    parser.add_argument(
        '-e', '--ensembl-rest',
        type=str,
        default='https://rest.ensembl.org',
        help="A URL to use for the ensembl rest API, to map Chembl target"
        " Uniprot IDs to Ensembl gene IDs, note this will only work for human"
        " proteins and defaults to build38"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argpare.ArgumentParser`
        The parser with the command line args detailed within it.

    Returns
    -------
    args : `Namespace`
        The parsed command line arguments.
    """
    # Now parse the arguments
    args = parser.parse_args()

    # If no database argument has been supplied then make the default argument
    # from the current date
    if args.db is None:
        args.db = get_default_bnf_db()

    # Make sure the full path is expanded in the config file
    args.config = utils.get_full_path(
        args.config, allow_none=True
    )
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_bnf(bnf_session, chembl_query=None, debug=None, verbose=False,
                 ensembl_rest_url='https://rest.ensembl.org', randdrug=False,
                 randwait=2.0, use_drugs=None, tmpdir=None):
    """Download the contents of the BNF website into a database.

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database.
    chembl_session : `sqlalchemy.Session`, optional, default: `NoneType`
        The session to use to interact with the ChEMBL database. If not
        provided then no ChEMBL mapping of drugs will happen.
    debug : `int`, optional, default: `NoneType`
        Only run for ``debug`` number of drugs. If not set then the whole
        lot are scraped.
    verbose : `bool`, optional, default: `False`
        Log progress.
    ensembl_rest_url : `str`, optional, default: `https://rest.ensembl.org`
        The URL to connect to the Ensembl REST API.
    randdrug : `bool`, optional, default: `False`
        Process the drugs in a random order.
    randwait : `float`, optional, default: `2.0`
        A max wait time for a random wait, so a time between 0-``random_wait``
        will be selected for each drug query.
    use_drugs : `list` of `str`
        Only process these specific drug entries. Note, debug is still applied
        to this list.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for the merge sort. If not provided then the
        default temp location is used.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    debug = debug or -1
    use_drugs = use_drugs or []

    # Make sure all the tables are created
    bo.Drug().metadata.create_all(bnf_session.get_bind())

    use_chembl = False
    restc = None
    if chembl_query is not None:
        use_chembl = True

        # Initialise the ensembl rest client
        restc = client.Rest(url=ensembl_rest_url)

    # Setup the objects that will be needed to interact with the BNF
    # website and the BNF database
    bnf_query = bq.BnfQuery(bnf_session)
    bnf_scrape = bnf.BnfScrape()

    bnf_date = bnf_scrape.get_update_date()
    logger.info(f"BNF website version: {bnf_date}")

    try:
        current_version = bnf_query.get_bnf_date()
        print(current_version)
    except NoResultFound:
        pass

    # Get all the drug links that we will need to navigate to and download
    # this could be empty if there are no new drugs and we have already
    # done a download session
    drug_links = get_drug_links(bnf_query, bnf_scrape)

    if len(use_drugs) > 0:
        use_drugs = [i.strip().lower() for i in use_drugs]
        debug_drugs = []
        for i in drug_links:
            if i.drug_name.lower() in use_drugs:
                debug_drugs.append(i)
        drug_links = debug_drugs

        if len(drug_links) == 0:
            raise ValueError(
                "can't find drugs: {0}".format(",".join(use_drugs))
            )

    # If we are dubugging the subset the number of records
    if debug > 0:
        drug_links = drug_links[:debug]

    if len(drug_links) > 0:
        # We have some drugs to download
        logger.info(f"will download {len(drug_links)} drugs")

        # If we are randomising order then suffle the list
        if randdrug is True:
            # This does inplace shuffling
            random.shuffle(drug_links)

        # Here random_wait is a small sleep time between web queries
        # so we do not hammer the webserver unnecessarily
        download_drugs(
            drug_links,
            bnf_session,
            bnf_scrape,
            random_wait=randwait,
            verbose=prog_verbose
        )
    else:
        # No drugs to download, what we do will depend on if we are mapping
        # to ChEMBL
        if use_chembl is True:
            logger.info("will check ChEMBL mappings, ...")
        else:
            logger.info("nothing to download...")
            return

    # Now if we have a CHEMBL connection we will attempt to map the drug names
    # to ChEMBL IDs and then onto targets
    # TODO: Deal with ADRENALINE/EPINEPHRINE in the drug components
    # TODO: Remember what the todo above was all about??
    if use_chembl is True:
        # If we are mapping to ChEMBL then create the database session
        # and start mapping
        map_to_chembl(bnf_query, chembl_query, verbose=prog_verbose)
        map_to_ensembl(
            bnf_query,
            restc,
            verbose=prog_verbose
        )

    # Now we create an index of all the drug effects
    nterms, ntokens, mterm_len, mtoken_len = build_index(
        bnf_query.session, verbose=prog_verbose, tmpdir=tmpdir
    )
    logger.info(f"# terms: {nterms}")
    logger.info(f"# tokens: {ntokens}")
    logger.info(f"max term length: {mterm_len}")
    logger.info(f"max token_length: {mtoken_len}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_default_bnf_db():
    """Return a default location to a BNF database that will be created.

    Returns
    -------
    db_name : `str`
        The default database name.

    Notes
    -----
    This is used when the user does not supply an output database argument. The
    database name is suffixed with the current date in YYYYMMDD format.
    """
    return os.path.join(
        os.environ['HOME'], "bnf_{0}.db".format(
            datetime.now().strftime("%Y%m%d")
        )
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_drug_links(bnf_query, bnf_scrape):
    """Get the drug links that we will need to download. These are the drugs
    that are not already in the database.

    Parameters
    ----------
    bnf_query : `bnf_download.queries.BnfQuery`
        A query object for the bnf database.
    bnf_scrape : `bnf_download.bnf.BnfScrape`
        A scrape object for the BNF website.

    Returns
    -------
    drug_links : `list` of `bnf_download.bnf.DrugLink`
        The drug link objects representing the drugs we want to download. If
        the list is empty then we already have the drugs in the database.
    """
    # Get the existing drug names, these will be cross referenced against the
    # current names in the BNF to see what needs downloading
    existing_drugs = [d.drug_name for d in bnf_query.get_all_drugs()]
    drug_links = bnf_scrape.get_drug_links()

    # Now make sure we remove any drugs that we already have
    return [dl for dl in drug_links if dl.drug_name not in existing_drugs]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_drugs(drug_links, session, bnf_scrape, random_wait=2,
                   verbose=False):
    """Download the drug data for the drugs represented in the drug links.

    Parameters
    ----------
    drug_links : `list` of `bnf_download.bnf.DrugLink`
        The drug link objects representing the drugs we want to download. If
        the list is empty then we already have the drugs in the database.
    session : `sqlalchemy.Session`
        The session to use to interact with the BNF database.
    bnf_scrape : `bnf_download.bnf.BnfScrape`
        A scrape object for the BNF website.
    random_wait : `int`, optional, default: `2`
        A max wait time for a random wait, so a time between 0-``random_wait``
        will be selected for each drug query.
    verbose : `bool`, optional, default: `False`
        Log progress.

    Notes
    -----
    This loads drug data directly to the database.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    with logging_redirect_tqdm(loggers=[logger]):
        tqdm_kwargs = dict(
            desc="[info] downloading, please wait...", disable=not verbose,
            unit=" drugs"
        )

        # loop through all the drugs and process
        for rowno, dl in enumerate(tqdm(drug_links, **tqdm_kwargs)):
            # Now parse the record
            drug_record = bnf_scrape.parse_drug(dl)
            # Create a database record for the drug
            drug = bo.Drug(drug_name=dl.drug_name, drug_url=dl.url)

            # Add the components
            for c in drug_record.components:
                bo.DrugComponent(drug=drug, drug_component_name=c)

            # Add the indications
            if len(drug_record.indications) > 0:
                for i, roa in drug_record.indications.items():
                    de = bo.DrugEffect(
                        drug=drug,
                        drug_effect_type='indication',
                        drug_effect_freq='all',
                        drug_effect_name=i
                    )
                    for r, dose in roa.items():
                        for patient, dose_desc in dose.items():
                            bo.DrugRoa(
                                drug_effect=de,
                                roa=r,
                                patient_group=patient,
                                dosage_desc=dose_desc
                            )
            else:
                logger.warning(
                    "0 indications for '{0}'".format(dl.url)
                )

            # Now the side effects
            if len(drug_record.side_effects) > 0:
                for s in drug_record.side_effects:
                    if s[1] is None:
                        print(drug)
                        pp.pprint(s)
                    bo.DrugEffect(drug=drug,
                                  drug_effect_type='side_effect',
                                  drug_effect_freq=s[0],
                                  drug_effect_name=s[1],
                                  drug_class_effect=s[2],
                                  drug_class=s[3])
            else:
                logger.warning(
                    "0 side_effects for '{0}'".format(dl.url)
                )

            for c in drug_record.contra_indications:
                bo.DrugEffect(
                    drug=drug,
                    drug_effect_type='contra_indication',
                    drug_effect_freq='all',
                    drug_effect_name=c
                )

            for c in drug_record.cautions:
                bo.DrugEffect(
                    drug=drug,
                    drug_effect_type='caution',
                    drug_effect_freq='all',
                    drug_effect_name=c
                )

            session.add(drug)
            if (rowno + 1) % 100 == 0:
                session.commit()

            # Wait a bit before the next request
            random_sleep(0, random_wait)

    # Final commit
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def random_sleep(lower_limit, upper_limit):
    """Sleep for a random amount of time between the lower limit and the upper
    limit.

    Parameters
    ----------
    lower_limit : `float`
        The lower limit to sleep.
    upper_limit : `float`
        The upper limit to sleep.

    Returns
    -------
    sleep_time : `float`
        The duration of the sleep.
    """

    sleep_time = random.uniform(float(lower_limit), float(upper_limit))

    # Perform the random sleep betwen both values
    time.sleep(sleep_time)

    return sleep_time


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def map_to_chembl(bnf_query, chembl_query, verbose=False):
    """Map the drug information into ChEMBL.

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database
    chembl_session : `sqlalchemy.Session`
        The session to use to query ChEMBL
    verbose : bool, optional, default: False
        Provide progress monitors

    Notes
    -----
    Each drug can have multiple components and it is the components that will
    be mapped back to ChEMBL. Mapping into ChEMBL can give uniform drug names
    and a stable ID. It also allows us to get the efficacy target.
    """
    # Each drug can have multiple components and it is the components that will
    # be mapped back to ChEMBL
    components = bnf_query.get_all_drug_components()

    # We know how many things we need to query so we can show remaining time
    # in the progress monitor
    tqdm_kwargs = dict(
        desc="[info] mapping to ChEMBL", unit=" drug components",
        disable=not verbose
    )

    # We only want to output a single example of each mapping. so we will
    # store the various ids of what we find.
    # i.e. the same component might be found in multiple drugs
    seen_mapped = set()

    # Loop through the components and attempt to map them to CHEMBL Ids
    for c in tqdm(components, **tqdm_kwargs):
        # This will do the mapping and will remove salts of the drug to
        # attempt to get a mapping
        mappings, exhaustive = chembl_query.map_drug_name(
            c.drug_component_name, exhaustive=True
        )

        if len(mappings) > 0:
            # Loop through all the mappings and write them to the database
            for m in mappings:
                # drug_component_id is the ID from the BNF database
                # compound_chembl_id is the CHEMBL ID that the drug maps to
                ids = (c.drug_component_id, m.compound_chembl_id)

                # If we have not processed this before then we do so now
                if ids not in seen_mapped:
                    # First we see if the component_id, chembl_id is
                    # already in the database, if so we will not add it
                    # again
                    chembl_mapping = find_existing_chembl_mapping(
                        bnf_query.session,
                        c.drug_component_id,
                        m.compound_chembl_id
                    )
                    seen_mapped.add(ids)
                    # print(m)
                    # print(m._fields)
                    # Only add to the database if the mapping is not already
                    # present
                    if len(chembl_mapping) == 0:
                        process_chembl_mapping(
                            bnf_query.session, chembl_query, c, m
                        )
    # Write any mappings to the database
    bnf_query.session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_existing_chembl_mapping(bnf_session, component_id, chembl_id):
    """
    Locate any existing link between `component_id`, `chembl_id` in the BNF
    database.

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database
    component_id : `sqlalchemy.Session`
        The drug component_id to check if it is associated with the ChEMBL ID
    chembl_id : str
        The ChEMBL ID to check if it has already been mapped to the
        component_id

    Returns
    -------
    chembl_mappings : list of `bnf_orm.ChemblMapping`
        Records from the database indicating that a mapping already exists in
        the database. This could be an empty list of no mapping exists
    """
    chembl_mapping = bnf_session.query(bo.ChemblMapping).\
        filter(bo.ChemblMapping.drug_component_id == component_id).\
        filter(bo.ChemblMapping.compound_chembl_id == chembl_id).\
        all()
    return chembl_mapping


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_chembl_mapping(bnf_session, chembl_query, drug_record,
                           chembl_mapping):
    """Associate a BNF drug record with a ChEMBL record and target

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database
    chembl_session : `sqlalchemy.Session`
        The session to use to query ChEMBL
    drug_record : `bnf_orm.DrugComponent`
        The drug component from th BNF database that needs to be linked to
        the ChemblMapping
    chembl_mapping : namedtuple
        The ChEMBL mapping that needs to be associated with the drug_decord
    """
    full_map = False

    try:
        # If the BNF drug name exactly matches the data in ChEMBL then we want
        # to flag that. i.e. we did not have to strip the salt from it to get
        # a match
        if drug_record.drug_component_name.lower() == \
           chembl_mapping.synonyms.lower() or \
           drug_record.drug_component_name.lower() == \
           chembl_mapping.compound_pref_name.lower():
            full_map = True
    except AttributeError:
        # Hmmm, not 100% sure what will be causing these, I can only think of
        # NoneTypes but I am not 100% sure why I would process these?
        pass

    # A ChEMBL mapping record in the BNF database
    chembl_mapping = bo.ChemblMapping(
        drug_component_id=drug_record.drug_component_id,
        full_mapping=full_map,
        mapping_synonym=chembl_mapping.synonyms,
        compound_pref_name=chembl_mapping.compound_pref_name,
        compound_chembl_id=chembl_mapping.compound_chembl_id,
        molecule_type=chembl_mapping.molecule_type,
        indication_class=chembl_mapping.indication_class)
    bnf_session.add(chembl_mapping)

    # Now map the drug targets
    drug_targets = chembl_query.find_drug_targets(
        chembl_mapping.compound_chembl_id
    )

    for dt, cid, rel in drug_targets:
        # uniprots.add(dt.uniprot_accession)
        ctc = bo.ChemblTargetComponents(
            chembl_mapping=chembl_mapping,
            target_mapping_compound_chembl_id=dt.compound_chembl_id,
            target_mapping_relationship=rel,
            mechanism_of_action=dt.mechanism_of_action,
            target_chembl_id=dt.target_chembl_id,
            target_type=dt.target_type,
            target_pref_name=dt.target_pref_name,
            organism=dt.organism,
            action_type=dt.action_type,
            target_accession=dt.target_accession,
            target_description=dt.target_desc)
        bnf_session.add(ctc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def map_to_ensembl(bnf_query, rest_client, verbose=False):
    """Associate a ENSEMBL Gene IDs to Uniprot IDs if the efficacy targets of
    BNF drugs that have been mapped to ChEMBL IDs

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database
    rest_client : `ensembl_rest_client.client.Rest`
        The ensembl rest client to use to map between Uniprot and Ensembl genes
    verbose : bool, optional, default: False
        Provide progress monitors
    """
    # Unique uniprot IDs
    uniprots = bnf_query.get_unique_target_accessions()

    # We know how many things we need to query so we can show remaining time
    # in the progress monitor
    tqdm_kwargs = dict(
        desc="[info] mapping using {0}".format(rest_client.url),
        unit=" drug components",
        disable=not verbose
    )

    # Obviously!
    species = 'human'
    object_type = 'gene'

    # Loop through the uniprots and attempt to map them to ENSEMBL Ids
    for u in tqdm(list(uniprots), **tqdm_kwargs):
        ensembl_mappings = rest_client.get_xrefs_symbol(
            species, u,
            object_type=object_type
        )

        if len(ensembl_mappings) > 0:
            ensembl_mappings = [em['id'] for em in ensembl_mappings]
            gene_data = rest_client.post_lookup_id(ensembl_mappings)

            for gene, gd in gene_data.items():
                eg = bo.EnsemblGenes(
                    target_accession=u,
                    ensembl_gene_id=gene,
                    chr_name=gd['seq_region_name'],
                    start_pos=gd['start'],
                    end_pos=gd['end'],
                    strand=gd['strand'],
                    assembly_name=gd['assembly_name'],
                    ensembl_description=gd['description'] or None
                )
                bnf_query.session.add(eg)
    bnf_query.session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_index(session, verbose=False, tmpdir=None):
    """Build index tables relating to the drug effects table

    Parameters
    ----------
    bnf_session : `sqlalchemy.Session`
        The session to use to interact with the BNF database.
    verbose : bool, optional, default: False
        Provide progress monitors.
    tmpdir : `str`, optional, default: `NoneType`
        The tmp directory to use for index creation.

    Returns
    -------
    nterms : `int`
        The total number of input terms processed.
    ntokens : `int`
        The total number of tokens generated.
    max_term_len : `int`
        The maximum input term length in characters.
    max_token_len : `int`
        The maximum token length in characters.
    """
    q = session.query(
        bo.DrugEffect.drug_effect_id,
        bo.DrugEffect.drug_effect_name
    ).order_by(bo.DrugEffect.drug_effect_id)
    return index.build_index(session, q, bo, tmpdir=tmpdir, verbose=verbose)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
