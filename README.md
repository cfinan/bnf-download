# bnf-download package

__version__: `0.2.0a0`

Classes and scripts for downloading (scraping) the drug indication and side effect information from the BNF database on the [BNF website](https://bnf.nice.org.uk/). This is necessary as no downloadable flat file exists. Note that this is the free version and not the paid one at [medicines complete](https://about.medicinescomplete.com/). This free version does not have any BNF chapters for example.

This package uses SQLAlchemy to manage the connections to all of the databases, as such, it is not restricted to any single database management system (DBMS). Having said that, currently it has only been tested against SQLite and MariaDB. I have attempted to test with duck-db but unfortunately I have not managed to get it working on any database.

Not only does it download the content from the BNF website. It also attempts to link the drug names back to `CHEMBL_ID`s. This then opens up the possibility for look ups on drug data. From the ChEMBL ID, the efficacy target for the drug is then extracted (as uniprot IDs). The uniprot IDs are mapped against Ensembl (using the REST API). to provide full drug->protein target->gene linkage.

The usage of ChEMBL is optional. If no ChEMBL database connection parameters are specified then no mapping to ChEMBL is attempted or Ensembl queries.

**This package is intended for research use only and not for any clinical applications**.

# Installation

There is [online](https://cfinan.gitlab.io/bnf-download/) documentation for bnf-download.

## Installation instructions
At present, bnf-download is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge bnf-download
```

There are currently builds for Python v3.8, v3.9 and v3.10 for Linux-64 and Mac-osx. Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/bnf-download.git
cd bnf-download
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.

## The config file
If you are using any other database other than SQLite or duck-db, you probably want to use a configuration file to specify your connection parameters to avoid giving passwords on the command line. By default, the download script will look for a config file called `~/db.cnf`. Here is an example below:

```
# Make sure no one else can see this file but you
# i.e.chmod o-rwx ~/.db.cnf

# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)

# The section headers can be called anything
[bnf_latest]
# All connection options here should be prefixed with db.
db.url=mysql+pymysql://user:password@hostname:3306/bnf

# You can still use SQLite databases from the config file
[chembl_27]
# All connection options here should be prefixed with db.
db.url=sqlite:////path/to/chembl_27.db

[chembl_25]
# All connection options here should be prefixed with db.
db.url=mysql+pymysql://user:password@hostname:3306/chembl_25
```

## The ChEMBL database
In order to map the BNF drug names to ChEMBL ids, a copy of the [ChEMBL](https://www.ebi.ac.uk/chembl/) database is needed. Although, it is quite large, this is not as bad as it seems as ChEMBL is distributed in a number of database formats, including SQLite. So it can be downloaded and used as a file without building in a DBMS. See [here](https://chembl.gitbook.io/chembl-interface-documentation/downloads). The SQLite version is about 19GB so is fairly hefty.

`bnf-download` has been tested and works against the MySQL and SQLite versions of ChEMBL. There is a ChEMBL REST API available and `bnf-download` may use that in future. However, this depends on how easy it is to do the drug matching.

## Change log

### version `0.2.0a0`
* SCRIPTS - `bnf-download` now works!
* DB - The version date is now stored in the database. You can see it on the [homepage](https://bnf.nice.org.uk/).
* DB - In addition to the indications and side effects the contra-indications and cautions free text is also stored now.
* DB - Added a new table for route of administration (ROA) data and dosage. Currently this is only in free text.
* DB - Added index tables for the `drug_effects` table.
* DB - Added some annotation tables in preparation for mapping drug_effects to clinical terminologies.
* API - Now uses a dedicated [ChEMBL-orm](https://gitlab.com/cfinan/chembl-orm) to perform the queries against ChEMBL and deprecated the `bnf_download.chembl_orm` module.
* API - Added drug effect index creation function `bnf_download.download.build_index`.
* DOCS - Added some documentation to the ORM schema.
