#!/bin/bash
DB="/home/rmjdcfi/bnf_20190714.db"
OUTFILE="../data/frailty.txt"
SORTED_OUT="../data/frailty_sorted.txt"

rm -f "$OUTFILE"

terms=(
    "diarrhoea"
    "anorexia"
    "abdominal pain"
    "constipation"
    "nausea"
    "vomiting"
    "dyspepsia"
    "glossitis" 
    "dry mouth"
    "taste disturbance"
    "dizziness"
    "syncope"
    "orthostatic hypotension"
    "worsening heart failure"
    "low mood"
    "dyspnoea"
    "bronchospasm"
    "vertigo"
    "headache"
    "sleep disturbance"
    "fatigue"
    "somnolence"
    "sedation"
    "malaise"
    "muscle fatigue"
    "asthenia"
    "muscle cramps"
    "myalgia"
    "arthralgia"
    "back pain"
    "parkinsonism"
    "peripheral oedema"
    "ataxia"
    "peripheral neuropathy"
    "memory"
    "confusion"
    "visual disturbance"
    "tinnitus"
    "hearing loss")

headers="on"
for i in "${terms[@]}"; do
    echo "$i"
sqlite3 "$DB" <<EOF >> "$OUTFILE"
.separator "\t"
.headers $headers
select 	dr.drug_id,
	dr.drug_name,
	dr.drug_url,
	de.drug_effect_type,
	de.drug_effect_freq,
	de.drug_effect_name,
	de.drug_class_effect,
	de.drug_class,
	GROUP_CONCAT(de2.drug_effect_name, "|") as drug_indication
from drug dr
join drug_effect de
on dr.drug_id = de.drug_id
join drug_effect de2
on dr.drug_id = de2.drug_id
where de.drug_effect_name like "%${i}%"
and de.drug_effect_type = "side_effect"
and de2.drug_effect_type = "indication"
group by dr.drug_id, de.drug_effect_id
order by de.drug_effect_type, de.drug_effect_freq
EOF
#break
headers="off"
done


terms=("fall" "falls")

for i in "${terms[@]}"; do
    echo "$i"
sqlite3 "$DB" <<EOF >> "$OUTFILE"
.separator "\t"
.headers off
select 	dr.drug_id,
	dr.drug_name,
	dr.drug_url,
	de.drug_effect_type,
	de.drug_effect_freq,
	de.drug_effect_name,
	de.drug_class_effect,
	de.drug_class,
	GROUP_CONCAT(de2.drug_effect_name, "|") as drug_indication
from drug dr
join drug_effect de
on dr.drug_id = de.drug_id
join drug_effect de2
on dr.drug_id = de2.drug_id
where de.drug_effect_name like "${i}"
and de.drug_effect_type = "side_effect"
and de2.drug_effect_type = "indication"
group by dr.drug_id, de.drug_effect_id
order by de.drug_effect_type, de.drug_effect_freq
EOF
#break
headers="off"
done

cat $OUTFILE | body sort -k2,2 -t$'\t' > "$SORTED_OUT"
tail -n+2 "$OUTFILE" | cut -f2 | sort | uniq -c | sort -k1,1rn | sed 's/^\s*//;s/\s/\t/' | awk 'BEGIN{FS="\t";OFS="\t";print "count","drug"}{$1=$1;print $0}' > ../data/frailty_counts.txt
tail -n+2 "$OUTFILE" | body grep "common_or_very_common" | cut -f2 | sort | uniq -c | sort -k1,1rn | sed 's/^\s*//;s/\s/\t/' | awk 'BEGIN{FS="\t";OFS="\t";print "count","drug"}{$1=$1;print $0}' > ../data/frailty_counts_common.txt
tail -n+2 "$OUTFILE" | cut -f6 | sort -u > ../data/unique_terms.txt
