#!/bin/bash
DB="../data/bnf_test.db"
OUTFILE="../data/blood_pressure_drugs.txt"

sqlite3 "$DB" <<EOF > "$OUTFILE"
.separator "\t"
.headers on
select * from drug dr
join drug_effect de
on dr.drug_id = de.drug_id
where drug_effect_name like "%hypertension%"
or drug_effect_name like "%hypotension%"
or drug_effect_name like "%blood pressure%"
order by drug_effect_type, drug_effect_freq
EOF
