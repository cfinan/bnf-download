#!/bin/bash
DB="../data/bnf_test.db"
OUTFILE="../data/diabetes_drugs.txt"

sqlite3 "$DB" <<EOF > "$OUTFILE"
.separator "\t"
.headers on
select * from drug dr
join drug_effect de
on dr.drug_id = de.drug_id
where drug_effect_name like "%glucose%"
or drug_effect_name like "%hyperglycemia%"
or drug_effect_name like "%hyperglycemic%"
or drug_effect_name like "%hypoglycemia%"
or drug_effect_name like "%hypoglycemic%"
or drug_effect_name like "%glycaemic%"
or drug_effect_name like "%glycaemia%"
or drug_effect_name like "%diabetes%"
or drug_effect_name like "%diabetic%"
order by drug_effect_type, drug_effect_freq
EOF
