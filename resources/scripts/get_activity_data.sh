#!/bin/bash

UNIPROT=(Q13275
         P19801
         Q96L34
         P48546
         Q9NS98
         Q9Y2I1
         Q30201
         P09917
         Q9UHC9
         P31327
         Q13133
         P08670
         P54219
         P08588
         Q14624
         P19827
         P29376
         Q9HBW0
         P10619
         Q02641
         P14618
         P55017
         P15151
         Q8WTV0
         O95342
         Q9UIF9
         O14733
         O75344
         A7E2Y1
           P37268
           P49841
           P04114
           Q14397
           P27708
           Q9Y6R4
           Q04609
           P11597
           Q9BV94
           Q02878
           Q8IW41
           P30040
           P19634
           P49771
           P02749
           P03372
           Q06418
           O43174
           Q96T55
           Q13356
           P55058
           P48637
           P14780
           Q9UNN8
           P25942
           P41235
           P02585
           Q9Y5X9
           O00626
           Q02127
           Q8NCC3
           P83111
           P55899
           Q09013
           O15382
           O75023
           P05981
           P08581
           Q16623
           P84098
           P09919
           P51570
           P01298
           P51452
           P19838
           P53779
           Q04756
           Q96C86
           Q11206
           Q6Q788
           P05091
           P42685
           O15244
           P15692
           P04035
           P63316
           P51957
           Q9UHY1
           P02751
           P09848
           Q01433
           Q15418
           Q96PL2
           Q49AN0
           P00747
           P46777
           Q9UP95
           Q9H2X9
           P12955
           Q96T54
           Q96DT6
           P43026
           Q9Y5R2
           P51532
           O94788
           P25786
           P01130
           Q92692
           P02649
           P02654
           O14594
           Q8N423
           Q9UJ14
           Q15466
           P04049
           P37231
           P16144
           P84243
           Q9Y5C1
           Q01432
           P09488
           P46439
           P21266
           Q9Y6L6
           P11117
           Q9Y4D2
           P51451
           O14672
           Q99674
           Q9Y289
           Q6UW56
           P58166
           Q6UW01
           Q9UK55
           P55081
           P40313
           Q9H4B8
           Q3B7J2
           P55786
           P04626
           P11086
           P31749
           Q9UBE0
           Q9H324
           Q86X55
           P62280
           P40429
           P28066
           Q9HCU4
           P51858
           Q10471
           Q7Z4P5
           O75469
           Q9P2G9
           Q9NRA1
           Q8IX30
           Q9BXY4
           O75751
           P62424
           Q12913
           Q8NBJ9
           Q9Y6M4
           P11245
           Q9BZJ8
           P32754
           O14905
           O14924
           P11801
           Q8IUL8
           Q76LX8
           Q9Y2K2
           Q16549
           Q6UXG3
           Q06033
           Q12923
           Q9BV23
           P55089
           Q86U86
           P30533
           P53582
           Q8NCB2
           Q04912
           Q8NCG7
           P07858
           Q96CH1
           O95477
           P17980
           P11150
           P06681
           Q3SYC2
           Q96P56
           Q8TBX8
           P56192
           P30101
           P19224
           Q9NYV4
           Q9H4A9
           Q6GTX8
           Q9BY76
           P22680
           Q9BQB4
           P48050
           P22105
           P39748
           Q03013
           Q8NBP7
           P08185
           Q8N6C8
           P46781
           P25090
           P21462
           Q9NPH6
           P06213
           Q14432
           Q6UWW8
           Q04206
           O95271
           Q16584
           Q9Y2U2
           O15245
           P55103
           O43916
           P06858
           Q56UN5
           Q9BXA6
           Q96GD4
           Q06124
           P00734
           Q8NGL9
           Q8NH70
           P07437
           Q7Z5Y6
           Q9NT22
           P09603
           O60733
           Q9P0J0
           P56817
           Q07869
           A6NI73
           P50895
           P25089
           Q86UW8
           P16403
           P18577
           O00144
           P01911
           P04229
           Q29974
           Q9GIY3
           P42330
           Q6UW10
           Q9BXC0
           P62805
           P11717
           P01009
           P11766
           P08319
           Q8NBQ5
           P12829
           P08519
           Q8NE28
           P11387
           Q96JM7
           P28062
           P01903
           Q99466
           Q15109
           Q96KQ7
           Q53GD3
           Q99519
           P08107
           O95866
           O95870
           O95445
           O14931
           Q06643
           P01375
           P10321
           Q6UXA7
           Q08345
           P17693
           P35410
           Q9GZK4
           Q9GZK7
           Q9BXB1
           P40306
           P01912
           P20039
           Q30134
           Q5Y7A7
           Q95IE3
           P28161
           P04180
           O75489
           Q2WEN9
           Q8TET4
           P01374
           P13760
           Q30167
           P13761
           Q9TQE0
           P08686
           P62857
           P30486
           P02655
           P01906
           P28065
           P59901
           P35504
           P13765
           O60656
           Q99944
           Q9HAW9
           P22309
           Q9HAW8
           P35503
           P00751
           Q9HAW7
           P22310
           Q6PI73
           P0C0L4
           P00738
           P05106
           P18621
           O95182)

OUTFILE=../data/activities.txt
rm -rf "$OUTFILE"
tail_value=1
for i in ${UNIPROT[@]}; do
    echo "$i"
    mysql --quick -uroot -pucl1t15cr@p -Dchembl_25 --host 127.0.0.1 --port 3311 <<EOF | tail -n+$tail_value >> "$OUTFILE"
    select 	md.pref_name as compound_pref_name
		, md.chembl_id as "compound_chembl_id"
		, md.max_phase
		, md.molecule_type
		, md.indication_class
		, td.chembl_id as "target_chembl_id"
		, td.pref_name as target_pref_name
		, td.target_type
		, td.organism
		, cs.component_type
		, cs.accession as uniprot_accession
		, cs.description as uniprot_description
		, s.chembl_id as assay_chembl_id
		, s.description as assay_description
		, ast.*
		, s.assay_test_type
		, s.assay_category
		, s.assay_organism
		, s.assay_strain
		, s.assay_tissue
		, s.assay_cell_type
		, rt.*
		, s.confidence_score
		, s.curated_by
		, a.standard_type
		, a.standard_relation
		, a.standard_value
		, a.standard_units
		, a.standard_flag
		, a.pchembl_value
		, a.activity_comment
		, a.standard_text_value
from molecule_dictionary md
join activities a
on a.molregno = md.molregno
join assays s
on s.assay_id = a.assay_id
join relationship_type rt
on rt.relationship_type = s.relationship_type
join assay_type ast
on ast.assay_type = s.assay_type
join target_dictionary td
on td.tid = s.tid
join target_components tc
on tc.tid = s.tid
join component_sequences cs
on cs.component_id = tc.component_id
where cs.accession = '${i}' and s.assay_type != "A"
EOF
    # break
    tail_value=2
done 
