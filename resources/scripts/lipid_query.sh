#!/bin/bash
DB="/home/rmjdcfi/bnf_20190713.db"
OUTFILE="../data/lipid_drugs.txt"

sqlite3 "$DB" <<EOF > "$OUTFILE"
.separator "\t"
.headers on
select * from drug dr
join drug_effect de
on dr.drug_id = de.drug_id
where drug_effect_name like "%lipid%"
or drug_effect_name like "%lipo%"
or drug_effect_name like "%cholesterol%"
or drug_effect_name like "%triglyceride%"
or drug_effect_name like "%HDL%"
or drug_effect_name like "%LDL%"
order by drug_effect_type, drug_effect_freq
EOF


OUTFILE="../data/lipid_drug_targets.txt"
sqlite3 "$DB" <<EOF > "$OUTFILE"
.separator "\t"
.headers on
select 	d.drug_id,
		d.drug_name,
		dc.drug_component_name,
		d.drug_url,
		de.drug_effect_type,
		de.drug_effect_freq,
		de.drug_effect_name,
		de.drug_class_effect,
		de.drug_class,
		cm.full_mapping,
		cm.compound_pref_name,
		cm.compound_chembl_id,
		cm.molecule_type,
		cm.indication_class,
		ctc.target_mapping_compound_chembl_id,
		ctc.target_mapping_relationship,
		ctc.mechanism_of_action,
		ctc.target_chembl_id,
		ctc.target_type,
		ctc.target_pref_name,
		ctc.organism,
		ctc.action_type,
		eg.uniprot_accession,
		ctc.uniprot_description,
		eg.ensembl_gene_id,
		eg.chr_name,
		eg.start_pos,
		eg.end_pos,
		eg.strand,
		eg.assembly_name,
		eg.ensembl_description
from drug d
join drug_effect de
on d.drug_id = de.drug_id
join drug_component dc
on dc.drug_id = d.drug_id
join chembl_mapping cm
on cm.drug_component_id = dc.drug_component_id
join chembl_target_components ctc
on ctc.compound_chembl_id = cm.compound_chembl_id
join ensembl_genes eg
on eg.uniprot_accession = ctc.uniprot_accession
where drug_effect_name LIKE "%lipid%"
or drug_effect_name LIKE "%lipo%"
or drug_effect_name like "%cholesterol%"
or drug_effect_name like "%triglyceride%"
or drug_effect_name like "%HDL%"
or drug_effect_name like "%LDL%"
EOF
