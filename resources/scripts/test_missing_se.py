#!/usr/bin/env python
from bnf_download import bnf_db, bnf_orm as bo, bnf

bnfs = bnf.BnfScrape()
dl = bnf.DrugLink('omeprazole', 'https://bnf.nice.org.uk/drug/omeprazole.html')

d = bnfs.parse_drug(dl)
print(d)
for i in d.side_effects:
    print(i)
