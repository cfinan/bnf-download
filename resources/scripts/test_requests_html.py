#!/usr/bin/env python
"""This test requests-html ability to render hidden java script on some of the
webpages.
"""
import requests
# from requests_html import HTMLSession
from lxml import html, etree
import re

dp = "https://bnf.nice.org.uk/drugs/tolvaptan/"

# session = HTMLSession()
# r = session.get(dp)
r = requests.get(dp)
tree = html.fromstring(r.content)
tree.make_links_absolute(r.url)
# indication_section_xpath = (
#     './/section[@aria-labelledby="indications-and-dose"]'
# )
indication_section_xpath = (
    './/section[@aria-labelledby="indications-and-dose"]'
    '/section[contains(@aria-labelledby, "-indications-and-dose")]'
)

# This will loop through different indication groups, each group has
# an indication string and one or more ROAs which can contain 1 or more
# patient groups
for g in tree.xpath(indication_section_xpath):
    # print(g)
    drug_name = re.sub(
        r'-indications-and-dose$', '', g.get('aria-labelledby')
    )
    print(drug_name)
    # print(g.xpath('./summary/h3/text()'))
    drug_brand = re.sub(r'[Ff]or\s+', '', " ".join(g.xpath('./details/summary/h3/text()')))
    print(drug_brand)
    is_brand = drug_brand.endswith('®')
    print(is_brand)
    # section[starts-with(@aria-labelledby, "{drug_name}-indication-")]
    for i in g.xpath(f'./details/section[starts-with(@aria-labelledby, "{drug_name}-indication-")]'):
        print(i.xpath('./h4/span/text()|./h3/span/text()'))
