#!/usr/bin/env python
# from bnf_download import bnf_db
from bnf_download import bnf
from umls import metamap
from pymisc import progress
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from sqlalchemy.orm import sessionmaker
import pprint as pp
import re


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def summarise(results):
    """
    Extract the pertinent info from the results
    """

    flattened = []
    for pc, p in enumerate(results):
        row = {'doc_no': pc}
        for uc, u in enumerate(p):
            row['utt_no'] = uc
            row['utt_text'] = u.text
            row['utt_coords'] = (u.start, u.end)
            for cph, ph in enumerate(u.phrases):
                row['ph_no'] = cph
                row['ph_text'] = ph.text
                row['ph_coords'] = (ph.start, ph.end)
                for mc, m in enumerate(ph.mappings):
                    row['map_no'] = mc
                    row['map_text'] = m.text
                    row['map_coords'] = (m.start, m.end)
                    for cc, c in enumerate(m.candidates):
                        row['cand_no'] = cc
                        row['cand_text'] = c.text
                        row['cand_coords'] = (c.start, c.end)
                        row['cuis'] = (c.cui, c.cui.stys)
                        flattened.append(row.copy())
    return flattened


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_specialist(effect):
    """
    There are some entries tagged as exper or specialist, in brackets, I will
    log these
    """

    # Substitute any specialist bracketed terms
    effect, subs = re.subn(r'\([\w\s]*(specialist|expert)[\w\s]*\)', '', effect)

    if subs > 0:
        return True, effect

    return False, effect


engine = create_engine('sqlite:////home/rmjdcfi/bnf_20190714.db')
Session = sessionmaker(bind=engine)
session = Session()

sql = text(
    """select group_concat(distinct drug_name), drug_effect_name from drug dr
    join drug_effect de
    on dr.drug_id = de.drug_id
    left join drug_effect_mapping dem
    on dem.drug_effect_id = de.drug_effect_id
    where dem.drug_effect_mapping_id IS NULL
    group by drug_effect_name
    order by length(de.drug_effect_name) desc"""
    )
# #    and de.drug_effect_name like '%with%'

rows = engine.execute(sql)
# q = bnf_db.BnfQuery(session)
# q.get_unmapped_effects()
args = ['-L', '2018AA', '-Z', '2018AA', '-sAIy', '--negex', '--conj', '-V', 'USAbase']
prog = progress.RateProgress(verbose=False)
with metamap.MetamapServer() as ms:
    # To map the effects to metamap
    mapper = bnf.BnfEffectMapper(ms)

    for r in prog.progress(rows):
        # Parse the effect to define meaning
        try:
            mapper.parse(r.drug_effect_name)
        except RuntimeError:
            pass
    # print("WITH LEADS")
    # pp.pprint(mapper.with_leads)
    # print("WITH LAGS")
    # pp.pprint(mapper.with_lags)
        # # print(r.drug_effect_name)

        # # Is the indication a specialist/expert one
        # is_spec, effect = is_specialist(r.drug_effect_name)
        # effect = effect.strip()
        # prog.static_msg(effect, prefix='[info]')
        # try:
        #     p = ms.run(effect, args)
        #     pp.pprint(summarise(p))

        #     # if is_spec is True:
        #     #     print(r.drug_effect_name)
        #     #     print(effect)
        #     #     pp.pprint(p)
        # except RuntimeError:
        #     pass
        # except ValueError:
        #     # parse errors e.g. gets turned into e . g and we can't locate the
        #     # substrings
        #     pass
